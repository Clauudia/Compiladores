import java.util.*;
public class Nodo{

  String valor;
  Nodo izquierdo;
  Nodo derecho;
  ArrayList<Nodo> hijos;

  public String getValor(){
    return this.valor;
  }

  public void setValor(String v){
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
    System.out.println("sah");
  }

  public void setHijo(Nodo h){
    this.hijos.add(h);
  }

  public void setHijosdeHijo(Nodo h){
    
  }

  public void imprime() {
    System.out.print("(" + this.valor);
    if(this.izquierdo != null)
      this.izquierdo.imprime();
    if(this.derecho != null)
      this.derecho.imprime();
    System.out.print(")");
  }

}
