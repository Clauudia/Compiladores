public class NodoAnd extends Nodo{

  public NodoAnd(String valor){
    this.valor = valor;
  }

  public NodoAnd(String valor, Nodo izq, Nodo der) {
    this.valor = valor;
    this.izquierdo = izq;
    this.derecho = der;
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
  }

}