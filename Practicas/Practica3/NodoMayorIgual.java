public class NodoMayorIgual extends Nodo{

  public NodoMayorIgual(String valor){
    this.valor = valor;
  }

  public NodoMayorIgual(String valor, Nodo izq, Nodo der) {
    this.valor = valor;
    this.izquierdo = izq;
    this.derecho = der;
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
  }

}