public class NodoMasIgual extends Nodo{

  public NodoMasIgual(String valor){
    this.valor = valor;
  }

  public NodoMasIgual(String valor, Nodo izq, Nodo der) {
    this.valor = valor;
    this.izquierdo = izq;
    this.derecho = der;
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
  }

}