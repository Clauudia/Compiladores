import java.util.*;

public class NodoRaiz extends Nodo{

  public NodoRaiz(String valor){
    this.valor = valor;
    this.hijos = new ArrayList<Nodo>();
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public void setHijo(Nodo h){
    this.hijos.add(h);
  }

  public void imprime(){
    System.out.print("(Raiz");
    for (Nodo hijo : this.hijos) {
      System.out.print("(");
      hijo.imprime();
      System.out.print(")");
    }
    System.out.print(")");
  }

}
