import java.util.*;

public class NodoauxIF extends Nodo{

  public NodoauxIF(String valor){
    this.valor = valor;
    this.hijos = new ArrayList<Nodo>();
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public void setHijo(Nodo h){
    this.hijos.add(h);
  }

  public void setHijosdeHijo(Nodo h){
    for (Nodo nd: h.hijos) {
      this.hijos.add(nd);
    }
  }

  public void imprime(){
    System.out.print("("+this.valor);
    for (Nodo hijo : this.hijos) {
      System.out.print("(");
      hijo.imprime();
      System.out.print(")");
    }
    System.out.print(")");
  }

}
