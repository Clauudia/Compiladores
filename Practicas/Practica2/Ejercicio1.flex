/********************************************************************************
**  Proyecto 1: Analizador léxico para p--, subconjunto de Python.                               **
**                                                                                                **
*********************************************************************************/
import java.util.Stack;
import java.util.Arrays;
%%
%public
%class Left
%byaccj
%line
%unicode
%debug
%{
    private Parser yyparser;

    /** Nuevo constructor
    * @param FileReader r
    * @param Parser parser - parser
    */
    public Left(java.io.Reader r, Parser parser){
    	   this(r);
    	   this.yyparser = parser;
    }

%}

F = [1-9][0-9]*|0


%%
[1-9][0-9]*|0                 { yyparser.yylval = new ParserVal(Integer.parseInt(yytext()));
                     return Parser.N;}                 
"/"                 { yyparser.yylval = new ParserVal(yytext());
                     return Parser.DIV;}
"*"                 { yyparser.yylval = new ParserVal(yytext());
                     return Parser.POR;}
"+"                 { yyparser.yylval = new ParserVal(yytext());
                     return Parser.MAS;}
"-"                 { yyparser.yylval = new ParserVal(yytext());
                     return Parser.MEN;}
("-")[1-9][0-9]*|0   { yyparser.yylval = new ParserVal(Integer.parseInt(yytext()));
                     return Parser.MN;} 
<<EOF>>           			{ return 0;}
[^]	{}