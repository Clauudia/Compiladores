Ejercicio1:
	¿Cuales són las maneras de implementar un analizador sintáctico descendente?
		Recursivo(llamando a subrutinas por cada no terminal) o predictivo(con una tabla de parseo)
	¿Que carácterísticas debe de cumplir una gramática libre del contexto para que pueden tener un reconocedor descendente recursivo sin caer en ciclos ni hacer backtrack?
		Que no tenga recursividad izquierda, que este factorizada y que ademas cumpla que es LL(k)
	El analizador se encuentra en el archivo tokens.flex
Ejercicio2:
	¿Qué resultado da la evaluación de la expresión 3-2+8? Explica el motivo de los resultados.
		En la primer gramatica(la recursiva izquierda), nos queda la penultima reduccion como Exp + term, lo que nos dice que hay precdencia a la izquierda ya que la resta se redujo primero.
		En la segunda gramatica la penultima reduccion es term - exp, lo que nos deja ver que la precedencia es a la derecha pues la suma se redujo primero.

Correr archivos con make right/left test=po.py
