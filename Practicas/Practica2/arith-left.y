%{
  import java.lang.Math;
  import java.io.*;
%} 
/* YACC Declarations */
%token MAS
%token MEN
%token POR
%token DIV
%token N
%token MN
%type<ival> EXP Start
%type<ival> TERM
%type<ival> FACT
%type<ival> N MN
/* Grammar follows  --- RIGHT RECURSION --- */
%%
Start: EXP                        { dump_stacks(stateptr);System.out.println("Reconocimiento exitoso de: "+$$);}
;
/* GRAMATICA  */
EXP:   EXP MAS TERM           {dump_stacks(stateptr);$$ = $1 + $3;}             
| EXP MEN TERM              {dump_stacks(stateptr);$$ = $1 - $3;}
| TERM                        {dump_stacks(stateptr);$$ = $1;}
;
TERM:   TERM POR FACT                       {dump_stacks(stateptr);$$ = $1 * $3;}
| TERM DIV FACT                           {dump_stacks(stateptr);$$ = $1 / $3;}
| FACT {dump_stacks(stateptr);$$ = $1 ;}
;
FACT:   N           {dump_stacks(stateptr);$$ = $1;}
| MN                {dump_stacks(stateptr);$$ = $1;}
;
%%
/* a reference to the lexer object */
private Left lexer;
/* interface to the lexer */
private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}
/* error reporting */ 
public void yyerror (String error) {
    System.err.println ("Error: " + error);
}
/* lexer is created in the constructor */
public Parser(Reader r) {
    lexer = new Left(r, this);
    yydebug = true;
}
/* that's how you use the parser */
public static void main(String args[]) throws IOException {
    Parser yyparser = new Parser(new FileReader(args[0]));
    yyparser.yyparse();    
}