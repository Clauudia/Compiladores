Preguntas

1.-¿Qué  es un analizador léxico?

-Es un programa que lee el código fuente de otro programa y lo divide en lexemas. Los lexemas son palabras reservadas de un cierto lenguaje.

2.-¿Cómo funcionan los generadores de analizadores léxicos?

-El esquema de un generador léxico se divide en 3 fases: 

La sección de definiciones; incluye declaraciones de variables, constantes y definiciones regulares que constituyen una manera cómoda de utilizar expresiones regulares largas.  

La sección de reglas; especifica los patrines  a reconocer y las acciones asociadas a estos.

la sección de rutinas; permite definir funciones auxiliares.

3.-¿Cuáles son los pasos que se sigue para obteber un AFD a partir de un conjunto de expresiones regulares?

-Pasar expresión regular a autómata finito no determinista.
-Construir cerrraduras epsilon.
-Eliminar transiciones múltiples. Función mover.
-Construir la cerradura de los estados que surgen de la función mover con cada uno de los caracteres.
-Continuar eliminació de transiciones multiples con la función mover.
