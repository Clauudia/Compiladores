/********************************************************************************
**                                                                                                **
*********************************************************************************/

%%

%public
%class Lexer
%standalone

LETRAA		=       a
LETRAB		=       b       
LETRAC      =       c


%%
{LETRAA} {System.out.println("CHAR[ a ]");}
{LETRAB} {System.out.println("CHAR[ b ]");}
{LETRAA}{LETRAB} | ({LETRAA}{LETRAB})*{LETRAC}		      	{ System.out.println("(CADENA VALIDA: "+yytext()+")");}