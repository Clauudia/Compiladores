/********************************************************************************
**                                                                                                **
*********************************************************************************/


NNDIGIT		=       [1-9]
DIGIT		=       [0-9]       
CERO        =        0
PUNTO		= 		.

LETRASMY	= [A-Z]
LETRASMN	= [a-z]
ESPECIALES	= $ | _


%%
{CERO} | {NNDIGIT}{DIGIT}*		      	{ System.out.println("INTEGER");}

{ESPECIALES}* | ({LETRASMY}* | {LETRASMN}*)+ | {DIGIT}* {System.out.println("IDENTIFICADOR");}

{CERO} | {NNDIGIT}{DIGIT}* 	| {PUNTO} | {CERO} | {NNDIGIT}{DIGIT}* {Systen.out.println("REAL");}