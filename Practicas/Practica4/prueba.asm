main:
.data
	x: .word 0
.text
lw $s0, x
li $s1 3
li $s2 9
add $s0, $s1 $s2
sw $s0, x
li $s2 0
COND0: blt $s0 $s2 ENDCOND0
li $s5 2
div $s0 $s5
mfhi $s3
li $s5 0
COND1: bne $s3 $s5 ENDCOND1
.data
temp0: .asciiz "par"
.text
li $v0 4
la $a0, temp0
syscall
j ENDCOND2
ENDCOND1: .data
temp1: .asciiz "impar"
.text
li $v0 4
la $a0, temp1
syscall
ENDCOND2: li $s7 1
sub $s0, $s0 $s7
j COND0
ENDCOND0: .data
temp2: .asciiz "fin"
.text
li $v0 4
la $a0, temp2
syscall
jr $ra
