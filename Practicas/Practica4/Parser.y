%{
  import java.lang.Math;
  import java.io.*;
%}
/* YACC Declarations */
%token NEWLINE IDENTIFIER ENTERO REAL CADENA S ENDOFFILE
%token BOOLEAN 
%token PRINT INDENT DEDENT
%token MAS MENOS POR POTENCIA DIV  /* + | - | * | ** | / */
%token DIVENTERA MODULO EQ /* // | % | =*/
%token MASIGUAL MENOSIGUAL DISTINTO IGUALIGUAL MENORIGUAL MAYORIGUAL MENOR MAYOR EQ
%token PYCOMA DOSPUNTOS PD PI 
%token IN ELIF ELSE IF WHILE FOR NOT OR AND NOTIN
%right ELIF ELSE
/* Grammar follows */
%%

file_input: { System.out.println("");}
|aux2 { root.acepta(v); g = new VisitanteGenerador(v.getTablaDeSimbolos()); root.acepta(g);}
;
aux2: NEWLINE
      |stmt {root.setHijo($1);}
      |aux2 NEWLINE
      |aux2 stmt {root.setHijo($2);}
;
stmt: simple_stmt {$$ = $1;}
      | compound_stmt {$$ = $1;}
;
simple_stmt: small_stmt NEWLINE {$$ = $1;}
            |small_stmt PYCOMA NEWLINE {$$ = $1;}
;
small_stmt: expr_stmt {$$ = $1;}
            | print_stmt {$$ = $1;}
;
expr_stmt: test {$$ = $1;}
  |test EQ test {$$ = $2;$2.setHijoIzq($1);$2.setHijoDer($3);}
    | test augassign test {$$ = $2;$2.setHijoIzq($1);$2.setHijoDer($3);}

;
augassign: MASIGUAL {$$ = $1;}
          | MENOSIGUAL {$$ = $1;}
;
print_stmt: PRINT test {$$ = $1;$1.setHijoDer($2);}
;
compound_stmt: if_stmt {$$ = $1;}
                | while_stmt {$$ = $1;}
;
/*if_stmt: 'if' test ':' suite ('elif' test ':' suite)* ['else' ':' suite]*/
if_stmt: IF test DOSPUNTOS suite {$$ = new NodoauxIF("IF"); $$.setHijo($1); $1.setHijoIzq($2); $1.setHijoDer($4);}
| IF test DOSPUNTOS suite ELSE DOSPUNTOS suite {$$ = new NodoauxIF("IF"); $$.setHijo($1); $1.setHijoIzq($2); $1.setHijoDer($4);$$.setHijo($5);$5.setHijoDer($7);}
          | IF test DOSPUNTOS suite aux20 ELSE DOSPUNTOS suite {$$ = new NodoauxIF("IF"); $$.setHijo($1); $1.setHijoIzq($2); $1.setHijoDer($4); $$.setHijoRecursivo($5); $$.setHijo($6); $6.setHijoDer($8);}
;
aux20: ELIF test DOSPUNTOS suite {$$ = new NodoauxIF("ELIF"); $1.setHijoIzq($2); $1.setHijoDer($4); $$.setHijo($1); }
      | ELIF test DOSPUNTOS suite aux20 {$$ = new NodoauxIF("ELIF"); $$.setHijo($1); $1.setHijoIzq($2); $1.setHijoDer($4); $$.setHijoRecursivo($5);}
;

while_stmt: WHILE test DOSPUNTOS suite {$$ = $1;$1.setHijoIzq($2);$1.setHijoDer($4);}
;
suite: simple_stmt {$$ = $1;}
      | NEWLINE INDENT aux00 DEDENT {$$ = $3;}
;

aux00: stmt {$$ = new NodoauxSuite("Suite"); $$.setHijo($1);}
| aux00 stmt {$$ = new NodoauxSuite("Suite"); $$.setHijoRecursivo($1);$$.setHijo($2);}
;
test: or_test {$$ = $1;}
      | PI test PD {$$ = $2;}
;
or_test: and_test {$$ = $1;}
          | and_test aux5 {$$ = $2; $2.setHijoIzq($1);}
;
aux5: OR and_test {$$ = $1; $1.setHijoDer($2);}
| aux5 OR and_test {$$ = $2; $2.setHijoDer($3);$2.setHijoIzq($1);}
;
and_test: not_test {$$ = $1;}
          | not_test aux6 {$$ = $2; $2.setHijoIzq($1);}
;
aux6: AND not_test {$$ = $1; $1.setHijoDer($2);}
      | aux6 AND not_test {$$ = $2; $2.setHijoDer($3);$2.setHijoIzq($1);}
;
not_test: NOT not_test {$$ = $1; $1.setHijoDer($2);}
          | comparison {$$ = $1;}
;
comparison: expr {$$ = $1;}
| expr aux7 {$$ = $2; $2.setHijoIzq($1);}
;
aux7: comp_op expr {$$ = $1; $1.setHijoDer($2);}
      | comp_op expr aux7 {$$ = $1; $3.setHijoIzq($2); $1.setHijoDer($3);}
;
comp_op: MENOR {$$ = $1;}
        | MAYOR {$$ = $1;}
        | IGUALIGUAL {$$ = $1;}
        | MAYORIGUAL {$$ = $1;}
        | MENORIGUAL {$$ = $1;}
        | DISTINTO {$$ = $1;}
        | IN {$$ = $1;}
        | NOTIN {$$ = $1;}
;
expr: term {$$ = $1;}
      | term aux8 {$$ = $2; $2.setHijoIzq($1);}
;
aux8: MAS term {$$ = $1; $1.setHijoDer($2);}
      | MENOS term {$$ = $1; $1.setHijoDer($2);}
      | MAS term aux8 {$$ = $1; $3.setHijoIzq($2); $1.setHijoDer($3);}
      | MENOS term aux8 {$$ = $1; $3.setHijoIzq($2); $1.setHijoDer($3);}
;
/* term: factor (('*'|'/'|'%'|'//') factor)* */
term:  factor {$$ = $1;}
      | factor aux9 {$$ = $2; $2.setHijoIzq($1);}
;
aux9:  POR factor {$$ = $1; $1.setHijoDer($2);}
     | DIVENTERA factor {$$ = $1; $1.setHijoDer($2);}
     | MODULO factor {$$ = $1; $1.setHijoDer($2);}
     | DIV factor {$$ = $1; $1.setHijoDer($2);}
     | aux9 POR factor {$$ = $2; $2.setHijoIzq($1); $2.setHijoDer($3);}
     | aux9 DIVENTERA factor {$$ = $1; $3.setHijoIzq($2); $1.setHijoDer($3);}
     | aux9 MODULO factor {$$ = $1; $3.setHijoIzq($2); $1.setHijoDer($3);}
     | aux9 DIV factor {$$ = $1; $3.setHijoIzq($2); $1.setHijoDer($3);}
;
/* factor: ('+'|'-') factor | power */
factor: MAS factor {$$ = $1; $1.setHijoIzq($2);}
| MENOS factor {$$ = $1; $1.setHijoIzq($2);}
  | power {$$ = $1;}
;
/* power: atom ['**' factor] */
power:  atom {$$ = $1;}
| atom POTENCIA factor {$$ = $2; $2.setHijoDer($3); $2.setHijoIzq($1);}
;

/* atom: IDENTIFIER | INTEGER | STRING | FLOAT */
atom:  IDENTIFIER {$$ = $1;}
     | ENTERO {$$ = $1;}
     | CADENA {$$ = $1;}
     | REAL {$$ = $1;}
     | BOOLEAN {$$ = $1;}
;
%%
/* a reference to the lexer object */
private Flexer lexer;
NodoRaiz root;
VisitantePrint v;
VisitanteGenerador g;

/* interface to the lexer */
private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}

/* error reporting */ 
public void yyerror (String error) {
    System.err.println ("Error: " + error);
}

/* lexer is created in the constructor */
public Parser(Reader r) {
    lexer = new Flexer(r, this);
    //yydebug = true;
    root = new NodoRaiz("root");
    v  = new VisitantePrint();
}

/* that's how you use the parser */
public static void main(String args[]) throws IOException {
    Parser yyparser = new Parser(new FileReader(args[0]));
    yyparser.yyparse();    
}