public class NodoEntero extends Nodo{

  public NodoEntero(String valor){
    this.valor = valor;
    this.tipo = "Entero";
  }

  public NodoEntero(String valor, Nodo izq, Nodo der) {
    this.valor = valor;
    this.izquierdo = izq;
    this.derecho = der;
    this.tipo = "Entero";
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
  }
public void acepta(Visitante v){
  v.visita(this);
}

}
