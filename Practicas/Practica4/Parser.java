//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 2 "Parser.y"
  import java.lang.Math;
  import java.io.*;
//#line 20 "Parser.java"




public class Parser
             implements ParserTokens
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//## **user defined:Nodo
String   yytext;//user variable to return contextual strings
Nodo yyval; //used to return semantic vals from action routines
Nodo yylval;//the 'lval' (result) I got from yylex()
Nodo valstk[] = new Nodo[YYSTACKSIZE];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
final void val_init()
{
  yyval=new Nodo();
  yylval=new Nodo();
  valptr=-1;
}
final void val_push(Nodo val)
{
  try {
    valptr++;
    valstk[valptr]=val;
  }
  catch (ArrayIndexOutOfBoundsException e) {
    int oldsize = valstk.length;
    int newsize = oldsize*2;
    Nodo[] newstack = new Nodo[newsize];
    System.arraycopy(valstk,0,newstack,0,oldsize);
    valstk = newstack;
    valstk[valptr]=val;
  }
}
final Nodo val_pop()
{
  return valstk[valptr--];
}
final void val_drop(int cnt)
{
  valptr -= cnt;
}
final Nodo val_peek(int relative)
{
  return valstk[valptr-relative];
}
final Nodo dup_yyval(Nodo val)
{
  return val;
}
//#### end semantic value section ####
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    0,    1,    1,    1,    1,    2,    2,    3,    3,
    5,    5,    6,    6,    6,    9,    9,    7,    4,    4,
   10,   10,   10,   13,   13,   11,   12,   12,   14,   14,
    8,    8,   15,   15,   17,   17,   16,   16,   19,   19,
   18,   18,   20,   20,   22,   22,   23,   23,   23,   23,
   23,   23,   23,   23,   21,   21,   25,   25,   25,   25,
   24,   24,   27,   27,   27,   27,   27,   27,   27,   27,
   26,   26,   26,   28,   28,   29,   29,   29,   29,   29,
};
final static short yylen[] = {                            2,
    0,    1,    1,    1,    2,    2,    1,    1,    2,    3,
    1,    1,    1,    3,    3,    1,    1,    2,    1,    1,
    4,    7,    8,    4,    5,    4,    1,    4,    1,    2,
    1,    3,    1,    2,    2,    3,    1,    2,    2,    3,
    2,    1,    1,    2,    2,    3,    1,    1,    1,    1,
    1,    1,    1,    1,    1,    2,    2,    2,    3,    3,
    1,    2,    2,    2,    2,    2,    3,    3,    3,    3,
    2,    2,    1,    1,    3,    1,    1,    1,    1,    1,
};
final static short yydefred[] = {                         0,
    3,   76,   77,   79,   78,   80,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    4,    7,    8,    0,   11,
   12,    0,   19,   20,   31,    0,    0,   42,    0,    0,
    0,   73,    0,   18,   71,   72,    0,    0,    0,   41,
    5,    6,    9,    0,    0,   16,   17,    0,    0,    0,
    0,    0,   52,   49,   51,   50,   47,   48,   53,   54,
   44,    0,    0,    0,   56,    0,    0,    0,    0,    0,
    0,   32,    0,    0,   10,   14,   15,   35,    0,   39,
    0,    0,    0,    0,   63,   66,   64,   65,    0,    0,
    0,    0,   75,    0,   27,    0,   26,   36,   40,   46,
   59,   60,   67,   70,   68,   69,    0,    0,    0,    0,
   29,    0,    0,    0,    0,   28,   30,    0,   22,    0,
    0,   23,   25,
};
final static short yydgoto[] = {                         14,
   15,   16,   95,   18,   19,   20,   21,   22,   48,   23,
   24,   96,  110,  112,   25,   26,   50,   27,   52,   28,
   29,   61,   62,   30,   65,   31,   70,   32,   33,
};
final static short yysindex[] = {                        92,
    0,    0,    0,    0,    0,    0,  167, -232, -232,  167,
  167,  167, -246,    0,  105,    0,    0,    0, -227,    0,
    0, -256,    0,    0,    0, -284, -271,    0,  177, -235,
 -194,    0, -236,    0,    0,    0, -241, -221, -216,    0,
    0,    0,    0, -208,  167,    0,    0,  167, -246, -224,
 -246, -237,    0,    0,    0,    0,    0,    0,    0,    0,
    0, -232, -232, -232,    0, -232, -232, -232, -232, -168,
 -232,    0,  143,  143,    0,    0,    0,    0, -246,    0,
 -246,  177, -235, -235,    0,    0,    0,    0, -232, -232,
 -232, -232,    0, -185,    0, -250,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,  155,  167, -213, -205,
    0,  -12, -193,  143, -190,    0,    0,  143,    0,  143,
 -192,    0,    0,
};
final static short yyrindex[] = {                        96,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   99,    0,    0,    0,    0,    0,
    0, -226,    0,    0,    0, -111,  -53,    0, -233,  -67,
 -127,    0, -157,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   37,
    0,  -50,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  -97,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0, -202,   21,   51,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    1,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
 -183,    0,    0,
};
final static short yygindex[] = {                         0,
    0,   -9,    2,    0,    0,    0,    0,   -7,    0,    0,
    0,  -64,  -13,    0,    0,  -33,    0,   -4,    0,    0,
   48,   50,    0,   -3,    3,   -1,    0,    0,    0,
};
final static int YYTABLESIZE=474;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         34,
   21,   17,   37,   38,   39,   42,   35,   36,   40,   97,
   49,    2,    3,    4,    5,   78,   17,    6,   45,   46,
   47,    8,    9,   43,   51,    2,    3,    4,    5,   43,
   13,    6,   63,   64,   71,    8,    9,   76,  108,  109,
   77,   43,   43,   43,   72,   98,   80,   13,   75,  119,
   43,   43,   43,  121,   45,  122,   44,   13,   81,   83,
   84,   43,   43,   73,   85,   86,   87,   88,   74,   93,
   79,  114,   45,   45,   45,   66,   99,   67,   68,   69,
  107,   45,   45,   45,  115,  101,  102,  103,  104,  105,
  106,  118,   45,   45,  120,    1,  108,  111,    2,   74,
  113,   89,  117,   90,   91,   92,   24,  123,   17,   82,
   74,   74,   74,   17,   74,   74,   74,   74,   74,   74,
   74,   74,   74,   74,   74,   74,   74,   74,   74,   61,
   74,  100,    0,    0,    0,    0,    0,   74,   74,   74,
   61,   61,    0,    0,    0,   33,    0,   61,   61,   61,
   61,   61,   61,   61,   61,   61,   61,   61,   61,   62,
   61,    0,    0,   33,   33,   33,    0,   61,   61,   61,
   62,   62,   33,   33,   33,    0,    0,   62,   62,   62,
   62,   62,   62,   62,   62,   62,   62,   62,   62,   55,
   62,    0,    0,    0,    0,    0,    0,   62,   62,   62,
    0,    0,    0,   37,    0,    0,   38,   55,   55,   55,
   55,   55,   55,   55,   55,   55,   55,   55,   55,    0,
   55,   37,   37,   37,   38,   38,   38,   55,   55,   55,
   37,   37,   37,   38,   38,   38,    0,    0,    0,    0,
    0,   37,    0,    0,   38,    2,    3,    4,    5,    0,
    0,    6,    7,    0,  116,    8,    9,   21,   21,   21,
   21,   21,    0,    0,   21,   21,    0,   21,   21,   21,
    0,    0,    0,    0,   10,    0,    0,   57,   11,   12,
    0,   13,    0,    0,    0,    0,    0,   21,    0,    0,
    0,   21,   21,   34,   21,   57,   57,   57,   57,   57,
   57,   57,   57,   57,   57,   57,   57,   58,   57,    0,
    0,   34,   34,   34,    0,   57,   57,   57,    0,    0,
   34,   34,   34,    0,    0,   58,   58,   58,   58,   58,
   58,   58,   58,   58,   58,   58,   58,    0,   58,    0,
    0,    0,    0,    0,    0,   58,   58,   58,    1,    2,
    3,    4,    5,    0,    0,    6,    7,    0,    0,    8,
    9,   41,    2,    3,    4,    5,    0,    0,    6,    7,
    0,    0,    8,    9,    0,    0,    0,    0,   10,    0,
    0,    0,   11,   12,    0,   13,    0,    0,    0,    0,
    0,   10,    0,    0,    0,   11,   12,    0,   13,   94,
    2,    3,    4,    5,    0,    0,    6,    7,    0,    0,
    8,    9,    2,    3,    4,    5,    0,    0,    6,    7,
    0,    0,    8,    9,    2,    3,    4,    5,    0,   10,
    6,    0,    0,    0,    8,    9,   13,    0,    0,    0,
    0,   10,    0,    0,    0,   11,   12,    0,   13,    0,
    0,    0,    0,   10,   53,   54,   55,   56,   57,   58,
   13,    0,    0,    0,   59,    0,    0,    0,    0,    0,
    0,    0,    0,   60,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                          7,
    0,    0,   10,   11,   12,   15,    8,    9,   13,   74,
  295,  258,  259,  260,  261,   49,   15,  264,  275,  276,
  277,  268,  269,  257,  296,  258,  259,  260,  261,  257,
  257,  264,  268,  269,  271,  268,  269,   45,  289,  290,
   48,  275,  276,  277,  286,   79,   51,  294,  257,  114,
  284,  285,  286,  118,  257,  120,  284,  284,  296,   63,
   64,  295,  296,  285,   66,   67,   68,   69,  285,   71,
  295,  285,  275,  276,  277,  270,   81,  272,  273,  274,
  266,  284,  285,  286,  290,   83,   84,   89,   90,   91,
   92,  285,  295,  296,  285,    0,  289,  107,    0,  257,
  108,  270,  112,  272,  273,  274,  290,  121,  107,   62,
  268,  269,  270,  112,  272,  273,  274,  275,  276,  277,
  278,  279,  280,  281,  282,  283,  284,  285,  286,  257,
  288,   82,   -1,   -1,   -1,   -1,   -1,  295,  296,  297,
  268,  269,   -1,   -1,   -1,  257,   -1,  275,  276,  277,
  278,  279,  280,  281,  282,  283,  284,  285,  286,  257,
  288,   -1,   -1,  275,  276,  277,   -1,  295,  296,  297,
  268,  269,  284,  285,  286,   -1,   -1,  275,  276,  277,
  278,  279,  280,  281,  282,  283,  284,  285,  286,  257,
  288,   -1,   -1,   -1,   -1,   -1,   -1,  295,  296,  297,
   -1,   -1,   -1,  257,   -1,   -1,  257,  275,  276,  277,
  278,  279,  280,  281,  282,  283,  284,  285,  286,   -1,
  288,  275,  276,  277,  275,  276,  277,  295,  296,  297,
  284,  285,  286,  284,  285,  286,   -1,   -1,   -1,   -1,
   -1,  295,   -1,   -1,  295,  258,  259,  260,  261,   -1,
   -1,  264,  265,   -1,  267,  268,  269,  257,  258,  259,
  260,  261,   -1,   -1,  264,  265,   -1,  267,  268,  269,
   -1,   -1,   -1,   -1,  287,   -1,   -1,  257,  291,  292,
   -1,  294,   -1,   -1,   -1,   -1,   -1,  287,   -1,   -1,
   -1,  291,  292,  257,  294,  275,  276,  277,  278,  279,
  280,  281,  282,  283,  284,  285,  286,  257,  288,   -1,
   -1,  275,  276,  277,   -1,  295,  296,  297,   -1,   -1,
  284,  285,  286,   -1,   -1,  275,  276,  277,  278,  279,
  280,  281,  282,  283,  284,  285,  286,   -1,  288,   -1,
   -1,   -1,   -1,   -1,   -1,  295,  296,  297,  257,  258,
  259,  260,  261,   -1,   -1,  264,  265,   -1,   -1,  268,
  269,  257,  258,  259,  260,  261,   -1,   -1,  264,  265,
   -1,   -1,  268,  269,   -1,   -1,   -1,   -1,  287,   -1,
   -1,   -1,  291,  292,   -1,  294,   -1,   -1,   -1,   -1,
   -1,  287,   -1,   -1,   -1,  291,  292,   -1,  294,  257,
  258,  259,  260,  261,   -1,   -1,  264,  265,   -1,   -1,
  268,  269,  258,  259,  260,  261,   -1,   -1,  264,  265,
   -1,   -1,  268,  269,  258,  259,  260,  261,   -1,  287,
  264,   -1,   -1,   -1,  268,  269,  294,   -1,   -1,   -1,
   -1,  287,   -1,   -1,   -1,  291,  292,   -1,  294,   -1,
   -1,   -1,   -1,  287,  278,  279,  280,  281,  282,  283,
  294,   -1,   -1,   -1,  288,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,  297,
};
}
final static short YYFINAL=14;
final static short YYMAXTOKEN=297;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"NEWLINE","IDENTIFIER","ENTERO","REAL","CADENA","S","ENDOFFILE",
"BOOLEAN","PRINT","INDENT","DEDENT","MAS","MENOS","POR","POTENCIA","DIV",
"DIVENTERA","MODULO","EQ","MASIGUAL","MENOSIGUAL","DISTINTO","IGUALIGUAL",
"MENORIGUAL","MAYORIGUAL","MENOR","MAYOR","PYCOMA","DOSPUNTOS","PD","PI","IN",
"ELIF","ELSE","IF","WHILE","FOR","NOT","OR","AND","NOTIN",
};
final static String yyrule[] = {
"$accept : file_input",
"file_input :",
"file_input : aux2",
"aux2 : NEWLINE",
"aux2 : stmt",
"aux2 : aux2 NEWLINE",
"aux2 : aux2 stmt",
"stmt : simple_stmt",
"stmt : compound_stmt",
"simple_stmt : small_stmt NEWLINE",
"simple_stmt : small_stmt PYCOMA NEWLINE",
"small_stmt : expr_stmt",
"small_stmt : print_stmt",
"expr_stmt : test",
"expr_stmt : test EQ test",
"expr_stmt : test augassign test",
"augassign : MASIGUAL",
"augassign : MENOSIGUAL",
"print_stmt : PRINT test",
"compound_stmt : if_stmt",
"compound_stmt : while_stmt",
"if_stmt : IF test DOSPUNTOS suite",
"if_stmt : IF test DOSPUNTOS suite ELSE DOSPUNTOS suite",
"if_stmt : IF test DOSPUNTOS suite aux20 ELSE DOSPUNTOS suite",
"aux20 : ELIF test DOSPUNTOS suite",
"aux20 : ELIF test DOSPUNTOS suite aux20",
"while_stmt : WHILE test DOSPUNTOS suite",
"suite : simple_stmt",
"suite : NEWLINE INDENT aux00 DEDENT",
"aux00 : stmt",
"aux00 : aux00 stmt",
"test : or_test",
"test : PI test PD",
"or_test : and_test",
"or_test : and_test aux5",
"aux5 : OR and_test",
"aux5 : aux5 OR and_test",
"and_test : not_test",
"and_test : not_test aux6",
"aux6 : AND not_test",
"aux6 : aux6 AND not_test",
"not_test : NOT not_test",
"not_test : comparison",
"comparison : expr",
"comparison : expr aux7",
"aux7 : comp_op expr",
"aux7 : comp_op expr aux7",
"comp_op : MENOR",
"comp_op : MAYOR",
"comp_op : IGUALIGUAL",
"comp_op : MAYORIGUAL",
"comp_op : MENORIGUAL",
"comp_op : DISTINTO",
"comp_op : IN",
"comp_op : NOTIN",
"expr : term",
"expr : term aux8",
"aux8 : MAS term",
"aux8 : MENOS term",
"aux8 : MAS term aux8",
"aux8 : MENOS term aux8",
"term : factor",
"term : factor aux9",
"aux9 : POR factor",
"aux9 : DIVENTERA factor",
"aux9 : MODULO factor",
"aux9 : DIV factor",
"aux9 : aux9 POR factor",
"aux9 : aux9 DIVENTERA factor",
"aux9 : aux9 MODULO factor",
"aux9 : aux9 DIV factor",
"factor : MAS factor",
"factor : MENOS factor",
"factor : power",
"power : atom",
"power : atom POTENCIA factor",
"atom : IDENTIFIER",
"atom : ENTERO",
"atom : CADENA",
"atom : REAL",
"atom : BOOLEAN",
};

//#line 138 "Parser.y"
/* a reference to the lexer object */
private Flexer lexer;
NodoRaiz root;
VisitantePrint v;
VisitanteGenerador g;

/* interface to the lexer */
private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}

/* error reporting */ 
public void yyerror (String error) {
    System.err.println ("Error: " + error);
}

/* lexer is created in the constructor */
public Parser(Reader r) {
    lexer = new Flexer(r, this);
    //yydebug = true;
    root = new NodoRaiz("root");
    v  = new VisitantePrint();
}

/* that's how you use the parser */
public static void main(String args[]) throws IOException {
    Parser yyparser = new Parser(new FileReader(args[0]));
    yyparser.yyparse();    
}
//#line 461 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 18 "Parser.y"
{ System.out.println("");}
break;
case 2:
//#line 19 "Parser.y"
{ root.acepta(v); g = new VisitanteGenerador(v.getTablaDeSimbolos()); root.acepta(g);}
break;
case 4:
//#line 22 "Parser.y"
{root.setHijo(val_peek(0));}
break;
case 6:
//#line 24 "Parser.y"
{root.setHijo(val_peek(0));}
break;
case 7:
//#line 26 "Parser.y"
{yyval = val_peek(0);}
break;
case 8:
//#line 27 "Parser.y"
{yyval = val_peek(0);}
break;
case 9:
//#line 29 "Parser.y"
{yyval = val_peek(1);}
break;
case 10:
//#line 30 "Parser.y"
{yyval = val_peek(2);}
break;
case 11:
//#line 32 "Parser.y"
{yyval = val_peek(0);}
break;
case 12:
//#line 33 "Parser.y"
{yyval = val_peek(0);}
break;
case 13:
//#line 35 "Parser.y"
{yyval = val_peek(0);}
break;
case 14:
//#line 36 "Parser.y"
{yyval = val_peek(1);val_peek(1).setHijoIzq(val_peek(2));val_peek(1).setHijoDer(val_peek(0));}
break;
case 15:
//#line 37 "Parser.y"
{yyval = val_peek(1);val_peek(1).setHijoIzq(val_peek(2));val_peek(1).setHijoDer(val_peek(0));}
break;
case 16:
//#line 40 "Parser.y"
{yyval = val_peek(0);}
break;
case 17:
//#line 41 "Parser.y"
{yyval = val_peek(0);}
break;
case 18:
//#line 43 "Parser.y"
{yyval = val_peek(1);val_peek(1).setHijoDer(val_peek(0));}
break;
case 19:
//#line 45 "Parser.y"
{yyval = val_peek(0);}
break;
case 20:
//#line 46 "Parser.y"
{yyval = val_peek(0);}
break;
case 21:
//#line 49 "Parser.y"
{yyval = new NodoauxIF("IF"); yyval.setHijo(val_peek(3)); val_peek(3).setHijoIzq(val_peek(2)); val_peek(3).setHijoDer(val_peek(0));}
break;
case 22:
//#line 50 "Parser.y"
{yyval = new NodoauxIF("IF"); yyval.setHijo(val_peek(6)); val_peek(6).setHijoIzq(val_peek(5)); val_peek(6).setHijoDer(val_peek(3));yyval.setHijo(val_peek(2));val_peek(2).setHijoDer(val_peek(0));}
break;
case 23:
//#line 51 "Parser.y"
{yyval = new NodoauxIF("IF"); yyval.setHijo(val_peek(7)); val_peek(7).setHijoIzq(val_peek(6)); val_peek(7).setHijoDer(val_peek(4)); yyval.setHijoRecursivo(val_peek(3)); yyval.setHijo(val_peek(2)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 24:
//#line 53 "Parser.y"
{yyval = new NodoauxIF("ELIF"); val_peek(3).setHijoIzq(val_peek(2)); val_peek(3).setHijoDer(val_peek(0)); yyval.setHijo(val_peek(3)); }
break;
case 25:
//#line 54 "Parser.y"
{yyval = new NodoauxIF("ELIF"); yyval.setHijo(val_peek(4)); val_peek(4).setHijoIzq(val_peek(3)); val_peek(4).setHijoDer(val_peek(1)); yyval.setHijoRecursivo(val_peek(0));}
break;
case 26:
//#line 57 "Parser.y"
{yyval = val_peek(3);val_peek(3).setHijoIzq(val_peek(2));val_peek(3).setHijoDer(val_peek(0));}
break;
case 27:
//#line 59 "Parser.y"
{yyval = val_peek(0);}
break;
case 28:
//#line 60 "Parser.y"
{yyval = val_peek(1);}
break;
case 29:
//#line 63 "Parser.y"
{yyval = new NodoauxSuite("Suite"); yyval.setHijo(val_peek(0));}
break;
case 30:
//#line 64 "Parser.y"
{yyval = new NodoauxSuite("Suite"); yyval.setHijoRecursivo(val_peek(1));yyval.setHijo(val_peek(0));}
break;
case 31:
//#line 66 "Parser.y"
{yyval = val_peek(0);}
break;
case 32:
//#line 67 "Parser.y"
{yyval = val_peek(1);}
break;
case 33:
//#line 69 "Parser.y"
{yyval = val_peek(0);}
break;
case 34:
//#line 70 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 35:
//#line 72 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 36:
//#line 73 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));val_peek(1).setHijoIzq(val_peek(2));}
break;
case 37:
//#line 75 "Parser.y"
{yyval = val_peek(0);}
break;
case 38:
//#line 76 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 39:
//#line 78 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 40:
//#line 79 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));val_peek(1).setHijoIzq(val_peek(2));}
break;
case 41:
//#line 81 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 42:
//#line 82 "Parser.y"
{yyval = val_peek(0);}
break;
case 43:
//#line 84 "Parser.y"
{yyval = val_peek(0);}
break;
case 44:
//#line 85 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 45:
//#line 87 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 46:
//#line 88 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 47:
//#line 90 "Parser.y"
{yyval = val_peek(0);}
break;
case 48:
//#line 91 "Parser.y"
{yyval = val_peek(0);}
break;
case 49:
//#line 92 "Parser.y"
{yyval = val_peek(0);}
break;
case 50:
//#line 93 "Parser.y"
{yyval = val_peek(0);}
break;
case 51:
//#line 94 "Parser.y"
{yyval = val_peek(0);}
break;
case 52:
//#line 95 "Parser.y"
{yyval = val_peek(0);}
break;
case 53:
//#line 96 "Parser.y"
{yyval = val_peek(0);}
break;
case 54:
//#line 97 "Parser.y"
{yyval = val_peek(0);}
break;
case 55:
//#line 99 "Parser.y"
{yyval = val_peek(0);}
break;
case 56:
//#line 100 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 57:
//#line 102 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 58:
//#line 103 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 59:
//#line 104 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 60:
//#line 105 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 61:
//#line 108 "Parser.y"
{yyval = val_peek(0);}
break;
case 62:
//#line 109 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 63:
//#line 111 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 64:
//#line 112 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 65:
//#line 113 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 66:
//#line 114 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 67:
//#line 115 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoIzq(val_peek(2)); val_peek(1).setHijoDer(val_peek(0));}
break;
case 68:
//#line 116 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 69:
//#line 117 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 70:
//#line 118 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 71:
//#line 121 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoIzq(val_peek(0));}
break;
case 72:
//#line 122 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoIzq(val_peek(0));}
break;
case 73:
//#line 123 "Parser.y"
{yyval = val_peek(0);}
break;
case 74:
//#line 126 "Parser.y"
{yyval = val_peek(0);}
break;
case 75:
//#line 127 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0)); val_peek(1).setHijoIzq(val_peek(2));}
break;
case 76:
//#line 131 "Parser.y"
{yyval = val_peek(0);}
break;
case 77:
//#line 132 "Parser.y"
{yyval = val_peek(0);}
break;
case 78:
//#line 133 "Parser.y"
{yyval = val_peek(0);}
break;
case 79:
//#line 134 "Parser.y"
{yyval = val_peek(0);}
break;
case 80:
//#line 135 "Parser.y"
{yyval = val_peek(0);}
break;
//#line 922 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
