import java.util.ArrayList;
import java.util.Set;
import java.io.*;
public class VisitanteGenerador implements Visitante{
TablaDeSimbolos t;
TablaDeSimbolos asigns;
int registros = 0;
int numjumps = 0;
int temp = 0;
File file;

public VisitanteGenerador(TablaDeSimbolos tabla){
  try{
     file = new File("prueba.asm"); //Your file
    FileOutputStream fos = new FileOutputStream(file);
    PrintStream ps = new PrintStream(fos);
    System.setOut(ps);
  }
  catch(Exception e){

  }
	t = tabla;
	asigns = new TablaDeSimbolos();
}


	public void visita(Nodo n){
		//System.out.print("Nodo Genérico");
	}

	public void visita(NodoAnd n){
    calcula(n);
	}

	public void visita(NodoBoolean n){
		System.out.print("");
	}

	public void visita(NodoCadena n){
		System.out.print("");
	}

	public void visita(NodoDistinto n){
	calcula(n);
	}

	public void visita(NodoDiv n){
    calcula(n);
	}

	public void visita(NodoDivEnt n){
	 calcula(n);
	}

	public void visita(NodoDosPuntos n){
		System.out.print("Nodo dos puntos");
	}

	public void visita(NodoElIf n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if (n.getHijoIzq() instanceof NodoID) {
        String hi = t.lookup(n.getHijoIzq().getValor());
        if(hi == null){
          System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          System.exit(1);
        }
      }
		System.out.print("Nodo elif");
	}

	public void visita(NodoElse n){
		String test = "";
    String suite = "";
    String cont = "";
    int temp = numjumps;
    //n.getHijoIzq().acepta(this);
    n.getHijoDer().acepta(this);
    System.out.print("ENDCOND"+temp+": ");
	}

	public void visita(NodoEntero n){
		System.out.print("Nodo entero");
	}

	public void visita(NodoIgual n){
    calcula(n);
	}

	public void visita(NodoID n){
    System.out.println("lw $s"+asigns.lookup(n.getValor())+" "+n.getValor());
	}

	public void visita(NodoIf n){
    String test = "";
    String suite = "";
    String cont = "";
    int temp = numjumps;
    n.getHijoIzq().acepta(this);
    n.getHijoDer().acepta(this);
    System.out.println("j ENDCOND"+(temp+1));
    System.out.print("ENDCOND"+temp+": ");
	}

	public void visita(NodoEq n){
    n.registro = registros;
    int r = registros;
    System.out.println("lw $s"+r+", "+n.getHijoIzq().getValor());
    n.getHijoDer().acepta(this);
    System.out.println("sw $s"+r+", "+n.getHijoIzq().getValor());
    asigns.insert(n.getHijoIzq().getValor(),String.valueOf(r));
    registros = 0;
	}

	public void visita(NodoIn n){
		System.out.print("Nodo In");
	}

	public void visita(NodoPrint n){
    int r = registros;
    if (n.getHijoDer() instanceof NodoID) {
      if(n.getHijoDer().getTipo().equals("Entero")){
        System.out.println("li $v0 1");
        System.out.println("la $a0, $s"+n.getValor());
      }
      else if(n.getHijoDer().getTipo().equals("Real")){
        System.out.println("li $v0 2");
        System.out.println("la $f12, $s"+n.getValor()); 
      }
      else if(n.getHijoDer().getTipo().equals("Cadena")){
        System.out.println("li $v0 4");
        System.out.println("la $a0, "+n.getValor());
      }
      else if(n.getHijoDer().getTipo().equals("Booleano")){
        System.out.println("li $v0 1");
        System.out.println("la $a0, $s"+n.getValor());
      }
    }
    else if(n.getHijoDer() instanceof NodoEntero){
      System.out.println("li $v0 1");
      System.out.println("li $a0, "+n.getHijoDer().getValor());
    }
    else if(n.getHijoDer() instanceof NodoReal){
      System.out.println("li $v0 2");
      System.out.println("li $f12, "+n.getHijoDer().getValor());
    }
    else if(n.getHijoDer() instanceof NodoBoolean){
      System.out.println("li $v0 1");
      if(n.getHijoDer().getValor().equals("True"))
        System.out.println("li $a0, 1");
      else
        System.out.println("li $a0, 0");
    }
    else if(n.getHijoDer() instanceof NodoCadena){
      System.out.println(".data");
      System.out.println("temp"+temp+": .asciiz \""+n.getHijoDer().getValor()+"\"");
      System.out.println(".text");
      System.out.println("li $v0 4");
      System.out.println("la $a0, temp"+temp); 
      temp++;
    }
    else{
      calcula(n.getHijoDer());
    }
		System.out.println("syscall");
	}

	public void visita(NodoMas n){
    calcula(n);
	}

	public void visita(NodoMasIgual n){
    calcula(n);
	}

	public void visita(NodoMayor n){
    calcula(n);
	}

	public void visita(NodoMayorIgual n){
    calcula(n);
	}

	public void visita(NodoMenor n){
    calcula(n);
	}

	public void visita(NodoMenorIgual n){
    calcula(n);
	}

	public void visita(NodoMenos n){
    calcula(n);
	}

	public void visita(NodoMenosIgual n){
    calcula(n);
	}

	public void visita(NodoMod n){
		calcula(n);
	}

	public void visita(NodoNewLine n){
		
	}

	public void visita(NodoNot n){
		System.out.print("");
	}

	public void visita(NodoNotIn n){
		System.out.print("");
	}

	public void visita(NodoOperador n){
		System.out.print("");
	}

	public void visita(NodoOr n){
    calcula(n);
	}

	public void visita(NodoPor n){
    calcula(n);
	}

	public void visita(NodoPot n){
    calcula(n);
	}

	public void visita(NodoPuntoyComa n){
		System.out.print("");
	}

	public void visita(NodoRaiz n){
    System.out.println("main:");
    System.out.println(".data");
    Set<String> set = t.tablaSim.keySet(); 
    for(String s : set){
      if(t.lookup(s).equals("Booleano"))
      System.out.println("\t"+s + ": .byte " + "0");
    else if(t.lookup(s).equals("Entero"))
      System.out.println("\t"+s + ": .word " + "0");
    else if(t.lookup(s).equals("Real"))
      System.out.println("\t"+s + ": .word " + "0");
    else
      System.out.println("\t"+s + ": .asciiz \"\"");
    //System.out.println("lw $t"+registros+", "+n.getHijoIzq().getValor());
    }
    System.out.println(".text");
		for(Nodo i : n.getHijos()){
			i.acepta(this);
		}
    System.out.println("jr $ra");
	}

	public void visita(NodoReal n){
		System.out.print("");
	}

	public void visita(NodoSeparador n){
		System.out.print("");
	}

	public void visita(NodoWhile n){
    String test = "";
    String suite = "";
    String cont = "";
    int temp = numjumps;
    n.getHijoIzq().acepta(this);
    n.getHijoDer().acepta(this);
    System.out.println("j COND"+temp);
    System.out.print("ENDCOND"+temp+": ");
	}

	public void visita(NodoauxIF n){
		n.acepta(this);
		System.out.print("");
	}

	public void visita(NodoauxSuite n){
		n.acepta(this);
		System.out.print("");
	}

  public void calcula(Nodo n){
    String etiqueta = "";
    if(n instanceof NodoID || n instanceof NodoBoolean || n instanceof NodoReal || n instanceof NodoEntero){
      if(n.registro == 12345)
          n.registro = registros;
      if(n instanceof NodoID)
        n.registro = Integer.parseInt(asigns.lookup(n.getValor()));
      else{
        System.out.println("li $s"+n.registro+" "+n.getValor());
      }
    }
    else{
      if(n.registro == 12345)
        n.registro = registros;
      registros++;
      calcula(n.getHijoIzq());
      registros++;
      calcula(n.getHijoDer());
      switch (n.getValor()) {
        case "+": 
            System.out.println("add $s"+n.registro+", $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            registros--;
            break;
        case "-": 
            System.out.println("sub $s"+n.registro+", $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            registros--;
            break;
        case "*": 
            System.out.println("mult $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            System.out.println("mflo $s"+n.registro);
            registros--;
            break;
        case "**":
            System.out.println("loop: beq $"+n.getHijoDer().registro+" $0 endloop");
            System.out.println("mult $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            System.out.println("mflo $s"+n.getHijoIzq().registro);
            System.out.println("j loop:");
            System.out.print("endloop:");
            registros--;
            break;
        case "/":
            System.out.println("move $f"+n.getHijoIzq().registro+" $s"+n.getHijoIzq().registro);
            System.out.println("move $f"+n.getHijoDer().registro+" $s"+n.getHijoDer().registro);  
            System.out.println("div.s $f"+n.registro+" $f"+n.getHijoIzq().registro+" $f"+n.getHijoDer().registro);
            registros--;
            break;
        case "//": 
            System.out.println("div $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            System.out.println("mflo $s"+n.registro);
            registros--;
            break;
        case "%": 
            System.out.println("div $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            System.out.println("mfhi $s"+n.registro);
            registros--;
            break;
        case "<": 
            etiqueta = "COND"+numjumps;
          System.out.println("bge $s"+asigns.lookup(n.getHijoIzq().getValor())+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++;
            break;
        case ">": 
          etiqueta = "COND"+numjumps;
          System.out.println("ble $s"+asigns.lookup(n.getHijoIzq().getValor())+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++;
            break;
        case "<=":
          etiqueta = "COND"+numjumps;
          System.out.println("bgt $s"+asigns.lookup(n.getHijoIzq().getValor())+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++; 
          //tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case ">=":
          etiqueta = "COND"+numjumps;
          //System.out.print(etiqueta+": ");
          System.out.println(etiqueta+": blt $s"+asigns.lookup(n.getHijoIzq().getValor())+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++; 
          //tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "==": 
          etiqueta = "COND"+numjumps;
          //System.out.print(etiqueta+": ");
          System.out.println(etiqueta+": bne $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++;
            break;
        case "!=": 
          etiqueta = "COND"+numjumps;
          //System.out.print(etiqueta+": ");
          System.out.println(etiqueta+": beq $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++;
            break;
        case "<>": 
          etiqueta = "COND"+numjumps;
          //System.out.print(etiqueta+": ");
          System.out.println(etiqueta+": beq $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro+" END"+etiqueta);
          numjumps++;
            break;
        case "and": 
          System.out.println("and $s"+n.registro+", $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            registros--;
            break;
        case "or": 
          System.out.println("or $s"+n.registro+", $s"+n.getHijoIzq().registro+" $s"+n.getHijoDer().registro);
            registros--;
            break;
        case "+=": 
          System.out.println("add $s"+asigns.lookup(n.getHijoIzq().getValor())+", $s"+asigns.lookup(n.getHijoIzq().getValor())+" $s"+n.getHijoDer().registro);
          //System.out.println("sw $s"+n.registro +" "+n.getHijoIzq().getValor());
          registros--;
            break;
        case "-=": 
          System.out.println("sub $s"+asigns.lookup(n.getHijoIzq().getValor())+", $s"+asigns.lookup(n.getHijoIzq().getValor())+" $s"+n.getHijoDer().registro);
          //System.out.println("sw $s"+n.registro +" "+n.getHijoIzq().getValor());
          registros--;
            break;
        default: 
          System.out.println("");
          break;
      }
    }
  }

}