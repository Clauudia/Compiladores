
import java.util.Stack;
import java.util.Arrays;
%%
%public
%class Flexer
%byaccj
%line
%state INDENT CADENA DEDENT NORMAL EOF
%unicode

%{

  // Parser
  private Parser yyparser;

  /**
  * Nuevo constructor
  * @param FileReader r
  * @param Parser parser - parser
  */
  public Flexer(java.io.Reader r, Parser parser){
    this(r);
    this.yyparser = parser;
  }

  // Variables auxiliares para la indentación
  static Stack<Integer> pila = new Stack<Integer>();
  static Integer actual = 0;
  static String cadena = "";
  static int dedents = 0;
  static int indents = 0;

  /**
  * Función que maneja los niveles de indentación y regresa átomos INDENT y DEDENT
  * @param int espacios - nivel de indetación actual.
  * @return void
  */
  public void indentacion(int espacios){
    if(pila.empty()){ //ponerle un cero a la pila si esta vacia
      pila.push(new Integer(0));             
    }

    Integer tope = pila.peek();

    if(tope != espacios){
      //Se debe emitir un DEDENT por cada nivel mayor al actual
      if(tope > espacios){
        while(pila.peek() > espacios &&  pila.peek()!=0 ){
          pila.pop();             
          dedents += 1;                                 
        }

        if(pila.peek() == espacios){
          yybegin(DEDENT);	       
        }else{
          System.out.println("IndentationError: line "+(yyline+1));
          System.exit(1);		    
        }

        return;
      }

      //El nivel actual de indentación es mayor a los anteriores.
      pila.push(espacios);
      yybegin(NORMAL);
      indents = 1;
    }else yybegin(NORMAL);
  }

%}

PUNTO         =	\.
DIGIT         = [0-9]
CERO          = 0+
ENTERO        = {CERO} | {DIGIT}+
REAL          = {ENTERO}? {PUNTO} {ENTERO}?
NEWLINE       = "\n"
IDENTIFIER    = ([:letter:] | "_")([:letter:] | "_" | [0-9])*
CHAR_LITERAL  = ([:letter:] | [:digit:] | "_" | "$" | " " | "#" | {OPERADOR} | {SEPARADOR})
OPERADOR      = ("+" | "-" | "*" | "**" | "/" | "//" | "%" | "<" | ">" | "<=" | "+=" | "-=" | ">=" | "==" | "!=" | "<>" | "=" )			
SEPARADOR     = ("(" | ")" | ":"  | ";" )
BOOLEAN       = ("True" | "False")
%%

<YYINITIAL>{
  (" " | "\t" )+[^" ""\t""#""\n"]   {System.out.println("IndentationError: Indent inesperado. Line "+(yyline+1));
                                     System.exit(1);}
  {NEWLINE}                         { }
  [^" ""\t"]                        {yypushback(1); yybegin(NORMAL);}   
}

<DEDENT>{
  .                                 {yypushback(1);
                                     if(dedents > 0){
                                      dedents--;
                                      return Parser.DEDENT;
                                     }
                                     yybegin(NORMAL);}
}

<CADENA>{
  {CHAR_LITERAL}+                   {cadena = yytext();}
  \"                                {yybegin(NORMAL);
                                      yyparser.yylval = new NodoCadena(cadena);
                                     return Parser.CADENA;}
  {NEWLINE}                         {System.out.println("Salto de linea inesperado. Line "+(yyline+1));
                                     System.exit(1);}
}

<NORMAL>{
  \"                                {yybegin(CADENA);}
  {REAL}                            {yyparser.yylval = new NodoReal(yytext()); return Parser.REAL;}
  {ENTERO}                          {yyparser.yylval = new NodoEntero(yytext()); return Parser.ENTERO;}

  "+"                               {yyparser.yylval = new NodoMas(yytext());  return Parser.MAS;}
  "-"                               {yyparser.yylval = new NodoMenos(yytext());  return Parser.MENOS;}
  "*"                               {yyparser.yylval = new NodoPor(yytext());  return Parser.POR;}
  "**"                              {yyparser.yylval = new NodoPot(yytext()); return Parser.POTENCIA;}
  "/"                               {yyparser.yylval = new NodoDiv(yytext());  return Parser.DIV;}
  "//"                              {yyparser.yylval = new NodoDivEnt(yytext());  return Parser.DIVENTERA;}
  "%"                               {yyparser.yylval = new NodoMod(yytext());  return Parser.MODULO;}
  "="                               {yyparser.yylval = new NodoEq(yytext());  return Parser.EQ;}
  "<"                               {yyparser.yylval = new NodoMenor(yytext()); return Parser.MENOR;}
  ">"                               {yyparser.yylval = new NodoMayor(yytext()); return Parser.MAYOR;}
  "=="                              {yyparser.yylval = new NodoIgual(yytext()); return Parser.IGUALIGUAL;}
  ">="                              {yyparser.yylval = new NodoMayorIgual(yytext()); return Parser.MAYORIGUAL;}
  "<="                              {yyparser.yylval = new NodoMenorIgual(yytext()); return Parser.MENORIGUAL;}
  "!="                              {yyparser.yylval = new NodoDistinto(yytext()); return Parser.DISTINTO;}
  "+="                              {yyparser.yylval = new NodoMasIgual(yytext()); return Parser.MASIGUAL;}
  "-="                              {yyparser.yylval = new NodoMenosIgual(yytext()); return Parser.MENOSIGUAL;}

  ";"                               {yyparser.yylval = new NodoPuntoyComa(yytext());return Parser.PYCOMA;}
  ":"                               {yyparser.yylval = new NodoDosPuntos(yytext());return Parser.DOSPUNTOS;}

  "in"                              {yyparser.yylval = new NodoIn(yytext());return Parser.IN;}
  "not in"                          {yyparser.yylval = new NodoNotIn(yytext());return Parser.NOTIN;}
  "if"                              {yyparser.yylval = new NodoIf(yytext());return Parser.IF;}
  "elif"                            {yyparser.yylval = new NodoElIf(yytext());return Parser.ELIF;}
  "else"                            {yyparser.yylval = new NodoElse(yytext());return Parser.ELSE;}
  "while"                           {yyparser.yylval = new NodoWhile(yytext());return Parser.WHILE;}
  "and"                             {yyparser.yylval = new NodoAnd(yytext());return Parser.AND;}
  "or"                              {yyparser.yylval = new NodoOr(yytext());return Parser.OR;}
  "not"                             {yyparser.yylval = new NodoNot(yytext());return Parser.NOT;}

{BOOLEAN}                             {yyparser.yylval = new NodoBoolean(yytext()); return Parser.BOOLEAN;}
  "print"                           {yyparser.yylval = new NodoKeyword(yytext()); return Parser.PRINT;}

  {IDENTIFIER}                        {yyparser.yylval = new NodoID(yytext()); return Parser.IDENTIFIER;}
  {NEWLINE}                           { yybegin(INDENT); 
                                          actual=0;
                                          yyparser.yylval = new NodoNewLine(yytext());
                                      return Parser.NEWLINE;}
  " "                               { }
}

<INDENT>{
  {NEWLINE}                         {actual = 0;}
  " "                               {actual++;}
  \t                                {actual += 4;}
  .                                 {yypushback(1);
                                     this.indentacion(actual);
                                     //yybegin(NORMAL);
                                     if(indents == 1){
                                      indents = 0;
                                      return Parser.INDENT;
                                     }
                                    }
}

<<EOF>>                             {this.indentacion(0);
                                     if(dedents > 0){
                                      dedents--;
                                      return Parser.DEDENT;
                                     }else{
                                      return 0;
                                     }
                                    }

[^] 					                      { }

