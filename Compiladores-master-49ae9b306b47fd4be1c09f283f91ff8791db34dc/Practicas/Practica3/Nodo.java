import java.util.*;
public class Nodo{

  String valor;
  Nodo izquierdo;
  Nodo derecho;
  ArrayList<Nodo> listaHijos; //lista para cargar las llamadas recursivas

  public String getValor(){
    return this.valor;
  }

  public void setValor(String v){
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
  }

  public void setHijo(Nodo h){
    this.listaHijos.add(h);
  }

  public void setHijoRecursivo(Nodo h){
    System.out.println("Recursion");
  }

  public void imprime() {
    System.out.print("[* " + this.valor);
    if(this.izquierdo != null)
      this.izquierdo.imprime();
    if(this.derecho != null)
      this.derecho.imprime();
    System.out.print(" *]");
  }

}
