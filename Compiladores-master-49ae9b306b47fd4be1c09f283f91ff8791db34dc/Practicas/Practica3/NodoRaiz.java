import java.util.*;

public class NodoRaiz extends Nodo{

  public NodoRaiz(String valor){
    this.valor = valor;
    this.listaHijos = new ArrayList<Nodo>();
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public void setHijo(Nodo h){
    this.listaHijos.add(h);
  }

  public void imprime(){
    System.out.print("(Raiz");
    for (Nodo h : this.listaHijos) {
      System.out.print("(");
      h.imprime();
      System.out.print(")");
    }
    System.out.print(")");
  }

}
