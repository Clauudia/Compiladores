//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 2 "Parser.y"
  import java.lang.Math;
  import java.io.*;
//#line 20 "Parser.java"




public class Parser
             implements ParserTokens
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//## **user defined:Nodo
String   yytext;//user variable to return contextual strings
Nodo yyval; //used to return semantic vals from action routines
Nodo yylval;//the 'lval' (result) I got from yylex()
Nodo valstk[] = new Nodo[YYSTACKSIZE];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
final void val_init()
{
  yyval=new Nodo();
  yylval=new Nodo();
  valptr=-1;
}
final void val_push(Nodo val)
{
  try {
    valptr++;
    valstk[valptr]=val;
  }
  catch (ArrayIndexOutOfBoundsException e) {
    int oldsize = valstk.length;
    int newsize = oldsize*2;
    Nodo[] newstack = new Nodo[newsize];
    System.arraycopy(valstk,0,newstack,0,oldsize);
    valstk = newstack;
    valstk[valptr]=val;
  }
}
final Nodo val_pop()
{
  return valstk[valptr--];
}
final void val_drop(int cnt)
{
  valptr -= cnt;
}
final Nodo val_peek(int relative)
{
  return valstk[valptr-relative];
}
final Nodo dup_yyval(Nodo val)
{
  return val;
}
//#### end semantic value section ####
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    0,    1,    1,    1,    1,    2,    2,    3,    5,
    5,    6,    6,    6,    9,    9,    7,    4,    4,   10,
   10,   10,   13,   13,   11,   12,   12,   14,   14,    8,
   15,   15,   17,   17,   16,   16,   19,   19,   18,   18,
   20,   20,   22,   22,   23,   23,   23,   23,   23,   23,
   23,   23,   21,   21,   25,   25,   25,   25,   24,   24,
   27,   27,   27,   27,   27,   27,   27,   27,   26,   26,
   26,   28,   28,   29,   29,   29,   29,   29,
};
final static short yylen[] = {                            2,
    0,    1,    1,    1,    2,    2,    1,    1,    2,    1,
    1,    1,    3,    3,    1,    1,    2,    1,    1,    4,
    7,    8,    4,    5,    4,    1,    4,    1,    2,    1,
    1,    2,    2,    3,    1,    2,    2,    3,    2,    1,
    1,    2,    2,    3,    1,    1,    1,    1,    1,    1,
    1,    1,    1,    2,    2,    2,    3,    3,    1,    2,
    2,    2,    2,    2,    3,    3,    3,    3,    2,    2,
    1,    1,    3,    1,    1,    1,    1,    1,
};
final static short yydefred[] = {                         0,
    3,   74,   75,   77,   76,   78,    0,    0,    0,    0,
    0,    0,    0,    0,    4,    7,    8,    0,   10,   11,
    0,   18,   19,   30,    0,    0,   40,    0,    0,    0,
   71,    0,   17,   69,   70,    0,    0,   39,    5,    6,
    9,    0,   15,   16,    0,    0,    0,    0,    0,   50,
   47,   49,   48,   45,   46,   51,   52,   42,    0,    0,
    0,   54,    0,    0,    0,    0,    0,    0,    0,    0,
   13,   14,   33,    0,   37,    0,    0,    0,    0,   61,
   64,   62,   63,    0,    0,    0,    0,   73,    0,   26,
    0,   25,   34,   38,   44,   57,   58,   65,   68,   66,
   67,    0,    0,    0,    0,   28,    0,    0,    0,    0,
   27,   29,    0,   21,    0,    0,   22,   24,
};
final static short yydgoto[] = {                         13,
   14,   15,   90,   17,   18,   19,   20,   21,   45,   22,
   23,   91,  105,  107,   24,   25,   47,   26,   49,   27,
   28,   58,   59,   29,   62,   30,   67,   31,   32,
};
final static short yysindex[] = {                        47,
    0,    0,    0,    0,    0,    0, -241,  139,  139, -241,
 -241, -241,    0,   60,    0,    0,    0, -247,    0,    0,
 -189,    0,    0,    0, -283, -281,    0,   52, -195, -248,
    0, -250,    0,    0,    0, -249, -218,    0,    0,    0,
    0, -241,    0,    0, -241, -241, -203, -241, -219,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  139,  139,
  139,    0,  139,  139,  139,  139, -227,  139,   98,   98,
    0,    0,    0, -241,    0, -241,   52, -195, -195,    0,
    0,    0,    0,  139,  139,  139,  139,    0, -172,    0,
 -209,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  122, -241, -188, -187,    0,  110, -184,   98, -183,
    0,    0,   98,    0,   98, -167,    0,    0,
};
final static short yyrindex[] = {                       105,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,  124,    0,    0,    0,    0,    0,    0,
 -130,    0,    0,    0, -243, -217,    0, -246,  -75, -132,
    0, -162,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0, -117,    0, -206,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0, -102,    0,    0,    0,
    0,    0,    0,    0,    0,    0, -220,  -48,    6,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    1,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0, -161,    0,    0,
};
final static short yygindex[] = {                         0,
    0,   -9,    2,    0,    0,    0,    0,   -7,    0,    0,
    0,  -61,   12,    0,    0,  -33,    0,   -4,    0,    0,
   71,   54,    0,   30,   21,   -2,    0,    0,    0,
};
final static int YYTABLESIZE=416;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         33,
   20,   16,   36,   37,   40,   34,   35,   38,   92,   41,
   41,   46,   73,   31,   48,   16,    2,    3,    4,    5,
   68,   63,    6,   64,   65,   66,    8,    9,   41,   41,
   41,   31,   31,   31,   71,   69,   43,   72,   41,   35,
   93,   31,   84,   75,   85,   86,   87,  114,   41,   41,
   36,  116,   12,  117,   43,   43,   43,   35,   35,   35,
   80,   81,   82,   83,   43,   88,   70,   35,   36,   36,
   36,   94,   60,   61,   43,   43,   76,   35,   36,  103,
  104,   98,   99,  100,  101,   42,   43,   44,   36,   78,
   79,   74,  106,  102,   72,  108,  109,  112,   96,   97,
  113,  115,  110,   16,    1,   72,   72,   72,   16,   72,
   72,   72,   72,   72,   72,   72,   72,   72,   72,   72,
   72,  103,   72,    2,   59,   72,   12,  118,   23,   77,
   95,    0,   72,   72,   72,   59,   59,    0,    0,   32,
    0,    0,   59,   59,   59,   59,   59,   59,   59,   59,
   59,    0,   59,    0,   60,   59,    0,   32,   32,   32,
    0,    0,   59,   59,   59,   60,   60,   32,    0,    0,
    0,    0,   60,   60,   60,   60,   60,   60,   60,   60,
   60,   53,   60,    0,    0,   60,    0,    0,    0,    0,
    0,    0,   60,   60,   60,    0,    0,    0,    0,   53,
   53,   53,   53,   53,   53,   53,   53,   53,   55,   53,
    0,    0,   53,    0,    0,    0,    0,    0,    0,   53,
   53,   53,    0,    0,    0,    0,   55,   55,   55,   55,
   55,   55,   55,   55,   55,    0,   55,    0,    0,   55,
    0,    0,    0,    0,    0,    0,   55,   55,   55,    0,
    0,    0,    0,    0,    0,    0,    0,   20,   20,   20,
   20,   20,   56,    0,   20,   20,    0,   20,   20,   20,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
   56,   56,   56,   56,   56,   56,   56,   56,   56,    0,
   56,   20,   20,   56,   20,    0,    0,    0,    0,    0,
   56,   56,   56,    1,    2,    3,    4,    5,    0,    0,
    6,    7,    0,    0,    8,    9,   39,    2,    3,    4,
    5,    0,    0,    6,    7,    0,    0,    8,    9,   50,
   51,   52,   53,   54,   55,    0,    0,   10,   11,   56,
   12,    0,    0,    0,    0,    0,    0,    0,   57,    0,
   10,   11,    0,   12,   89,    2,    3,    4,    5,    0,
    0,    6,    7,    0,    0,    8,    9,    2,    3,    4,
    5,    0,    0,    6,    7,    0,  111,    8,    9,    2,
    3,    4,    5,    0,    0,    6,    7,    0,    0,    8,
    9,   12,    0,    0,    0,    0,    2,    3,    4,    5,
   10,   11,    6,   12,    0,    0,    8,    9,    0,    0,
    0,    0,   10,   11,    0,   12,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                          7,
    0,    0,   10,   11,   14,    8,    9,   12,   70,  257,
  257,  295,   46,  257,  296,   14,  258,  259,  260,  261,
  271,  270,  264,  272,  273,  274,  268,  269,  275,  276,
  277,  275,  276,  277,   42,  285,  257,   45,  285,  257,
   74,  285,  270,   48,  272,  273,  274,  109,  295,  296,
  257,  113,  294,  115,  275,  276,  277,  275,  276,  277,
   63,   64,   65,   66,  285,   68,  285,  285,  275,  276,
  277,   76,  268,  269,  295,  296,  296,  295,  285,  289,
  290,   84,   85,   86,   87,  275,  276,  277,  295,   60,
   61,  295,  102,  266,  257,  103,  285,  107,   78,   79,
  285,  285,  290,  102,    0,  268,  269,  270,  107,  272,
  273,  274,  275,  276,  277,  278,  279,  280,  281,  282,
  283,  289,  285,    0,  257,  288,  257,  116,  290,   59,
   77,   -1,  295,  296,  297,  268,  269,   -1,   -1,  257,
   -1,   -1,  275,  276,  277,  278,  279,  280,  281,  282,
  283,   -1,  285,   -1,  257,  288,   -1,  275,  276,  277,
   -1,   -1,  295,  296,  297,  268,  269,  285,   -1,   -1,
   -1,   -1,  275,  276,  277,  278,  279,  280,  281,  282,
  283,  257,  285,   -1,   -1,  288,   -1,   -1,   -1,   -1,
   -1,   -1,  295,  296,  297,   -1,   -1,   -1,   -1,  275,
  276,  277,  278,  279,  280,  281,  282,  283,  257,  285,
   -1,   -1,  288,   -1,   -1,   -1,   -1,   -1,   -1,  295,
  296,  297,   -1,   -1,   -1,   -1,  275,  276,  277,  278,
  279,  280,  281,  282,  283,   -1,  285,   -1,   -1,  288,
   -1,   -1,   -1,   -1,   -1,   -1,  295,  296,  297,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  257,  258,  259,
  260,  261,  257,   -1,  264,  265,   -1,  267,  268,  269,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  275,  276,  277,  278,  279,  280,  281,  282,  283,   -1,
  285,  291,  292,  288,  294,   -1,   -1,   -1,   -1,   -1,
  295,  296,  297,  257,  258,  259,  260,  261,   -1,   -1,
  264,  265,   -1,   -1,  268,  269,  257,  258,  259,  260,
  261,   -1,   -1,  264,  265,   -1,   -1,  268,  269,  278,
  279,  280,  281,  282,  283,   -1,   -1,  291,  292,  288,
  294,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  297,   -1,
  291,  292,   -1,  294,  257,  258,  259,  260,  261,   -1,
   -1,  264,  265,   -1,   -1,  268,  269,  258,  259,  260,
  261,   -1,   -1,  264,  265,   -1,  267,  268,  269,  258,
  259,  260,  261,   -1,   -1,  264,  265,   -1,   -1,  268,
  269,  294,   -1,   -1,   -1,   -1,  258,  259,  260,  261,
  291,  292,  264,  294,   -1,   -1,  268,  269,   -1,   -1,
   -1,   -1,  291,  292,   -1,  294,
};
}
final static short YYFINAL=13;
final static short YYMAXTOKEN=297;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"NEWLINE","IDENTIFIER","ENTERO","REAL","CADENA","S","ENDOFFILE",
"BOOLEAN","PRINT","INDENT","DEDENT","MAS","MENOS","POR","POTENCIA","DIV",
"DIVENTERA","MODULO","EQ","MASIGUAL","MENOSIGUAL","DISTINTO","IGUALIGUAL",
"MENORIGUAL","MAYORIGUAL","MENOR","MAYOR","PYCOMA","DOSPUNTOS","PD","PI","IN",
"ELIF","ELSE","IF","WHILE","FOR","NOT","OR","AND","NOTIN",
};
final static String yyrule[] = {
"$accept : file_input",
"file_input :",
"file_input : aux2",
"aux2 : NEWLINE",
"aux2 : stmt",
"aux2 : aux2 NEWLINE",
"aux2 : aux2 stmt",
"stmt : simple_stmt",
"stmt : compound_stmt",
"simple_stmt : small_stmt NEWLINE",
"small_stmt : expr_stmt",
"small_stmt : print_stmt",
"expr_stmt : test",
"expr_stmt : test EQ test",
"expr_stmt : test augassign test",
"augassign : MASIGUAL",
"augassign : MENOSIGUAL",
"print_stmt : PRINT test",
"compound_stmt : if_stmt",
"compound_stmt : while_stmt",
"if_stmt : IF test DOSPUNTOS suite",
"if_stmt : IF test DOSPUNTOS suite ELSE DOSPUNTOS suite",
"if_stmt : IF test DOSPUNTOS suite aux20 ELSE DOSPUNTOS suite",
"aux20 : ELIF test DOSPUNTOS suite",
"aux20 : ELIF test DOSPUNTOS suite aux20",
"while_stmt : WHILE test DOSPUNTOS suite",
"suite : simple_stmt",
"suite : NEWLINE INDENT aux00 DEDENT",
"aux00 : stmt",
"aux00 : aux00 stmt",
"test : or_test",
"or_test : and_test",
"or_test : and_test aux5",
"aux5 : OR and_test",
"aux5 : aux5 OR and_test",
"and_test : not_test",
"and_test : not_test aux6",
"aux6 : AND not_test",
"aux6 : aux6 AND not_test",
"not_test : NOT not_test",
"not_test : comparison",
"comparison : expr",
"comparison : expr aux7",
"aux7 : comp_op expr",
"aux7 : comp_op expr aux7",
"comp_op : MENOR",
"comp_op : MAYOR",
"comp_op : IGUALIGUAL",
"comp_op : MAYORIGUAL",
"comp_op : MENORIGUAL",
"comp_op : DISTINTO",
"comp_op : IN",
"comp_op : NOTIN",
"expr : term",
"expr : term aux8",
"aux8 : MAS term",
"aux8 : MENOS term",
"aux8 : MAS term aux8",
"aux8 : MENOS term aux8",
"term : factor",
"term : factor aux9",
"aux9 : POR factor",
"aux9 : DIVENTERA factor",
"aux9 : MODULO factor",
"aux9 : DIV factor",
"aux9 : aux9 POR factor",
"aux9 : aux9 DIVENTERA factor",
"aux9 : aux9 MODULO factor",
"aux9 : aux9 DIV factor",
"factor : MAS factor",
"factor : MENOS factor",
"factor : power",
"power : atom",
"power : atom POTENCIA factor",
"atom : IDENTIFIER",
"atom : ENTERO",
"atom : CADENA",
"atom : REAL",
"atom : BOOLEAN",
};

//#line 136 "Parser.y"
/* a reference to the lexer object */
private Flexer lexer;
NodoRaiz root;

/* interface to the lexer */
private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}

/* error reporting */ 
public void yyerror (String error) {
    System.err.println ("Error: " + error);
}

/* lexer is created in the constructor */
public Parser(Reader r) {
    lexer = new Flexer(r, this);
    yydebug = true;
    root = new NodoRaiz("root");
}

/* that's how you use the parser */
public static void main(String args[]) throws IOException {
    Parser yyparser = new Parser(new FileReader(args[0]));
    yyparser.yyparse();    
}
//#line 441 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 1:
//#line 18 "Parser.y"
{ System.out.println("Reconocimiento exitoSO");}
break;
case 2:
//#line 19 "Parser.y"
{root.imprime(); System.out.println("Reconocimiento exitoso");}
break;
case 4:
//#line 22 "Parser.y"
{root.setHijo(val_peek(0));}
break;
case 6:
//#line 24 "Parser.y"
{root.setHijo(val_peek(0));}
break;
case 7:
//#line 26 "Parser.y"
{yyval = val_peek(0);}
break;
case 8:
//#line 27 "Parser.y"
{yyval = val_peek(0);}
break;
case 9:
//#line 29 "Parser.y"
{yyval = val_peek(1);}
break;
case 10:
//#line 31 "Parser.y"
{yyval = val_peek(0);}
break;
case 11:
//#line 32 "Parser.y"
{yyval = val_peek(0);}
break;
case 12:
//#line 34 "Parser.y"
{yyval = val_peek(0);}
break;
case 13:
//#line 35 "Parser.y"
{yyval = val_peek(1);val_peek(1).setHijoIzq(val_peek(2));val_peek(1).setHijoDer(val_peek(0));}
break;
case 14:
//#line 36 "Parser.y"
{yyval = val_peek(1);val_peek(1).setHijoIzq(val_peek(2));val_peek(1).setHijoDer(val_peek(0));}
break;
case 15:
//#line 39 "Parser.y"
{yyval = val_peek(0);}
break;
case 16:
//#line 40 "Parser.y"
{yyval = val_peek(0);}
break;
case 17:
//#line 42 "Parser.y"
{yyval = val_peek(1);val_peek(1).setHijoDer(val_peek(0));}
break;
case 18:
//#line 44 "Parser.y"
{yyval = val_peek(0);}
break;
case 19:
//#line 45 "Parser.y"
{yyval = val_peek(0);}
break;
case 20:
//#line 48 "Parser.y"
{yyval = new NodoauxIF("IF"); yyval.setHijo(val_peek(3)); val_peek(3).setHijoIzq(val_peek(2)); val_peek(3).setHijoDer(val_peek(0));}
break;
case 21:
//#line 49 "Parser.y"
{yyval = new NodoauxIF("IF"); yyval.setHijo(val_peek(6)); val_peek(6).setHijoIzq(val_peek(5)); val_peek(6).setHijoDer(val_peek(3));yyval.setHijo(val_peek(2));val_peek(2).setHijoDer(val_peek(0));}
break;
case 22:
//#line 50 "Parser.y"
{yyval = new NodoauxIF("IF"); yyval.setHijo(val_peek(7)); val_peek(7).setHijoIzq(val_peek(6)); val_peek(7).setHijoDer(val_peek(4)); yyval.setHijoRecursivo(val_peek(3)); yyval.setHijo(val_peek(2)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 23:
//#line 52 "Parser.y"
{yyval = new NodoauxIF("ELIF"); val_peek(3).setHijoIzq(val_peek(2)); val_peek(3).setHijoDer(val_peek(0)); yyval.setHijo(val_peek(3)); }
break;
case 24:
//#line 53 "Parser.y"
{yyval = new NodoauxIF("ELIF"); yyval.setHijo(val_peek(4)); val_peek(4).setHijoIzq(val_peek(3)); val_peek(4).setHijoDer(val_peek(1)); yyval.setHijoRecursivo(val_peek(0));}
break;
case 25:
//#line 56 "Parser.y"
{yyval = val_peek(3);val_peek(3).setHijoIzq(val_peek(2));val_peek(3).setHijoDer(val_peek(0));}
break;
case 26:
//#line 58 "Parser.y"
{yyval = val_peek(0);}
break;
case 27:
//#line 59 "Parser.y"
{yyval = val_peek(1);}
break;
case 28:
//#line 62 "Parser.y"
{yyval = new NodoauxSuite("Suite"); yyval.setHijo(val_peek(0));}
break;
case 29:
//#line 63 "Parser.y"
{yyval = new NodoauxSuite("Suite"); yyval.setHijoRecursivo(val_peek(1));yyval.setHijo(val_peek(0));}
break;
case 30:
//#line 65 "Parser.y"
{yyval = val_peek(0);}
break;
case 31:
//#line 67 "Parser.y"
{yyval = val_peek(0);}
break;
case 32:
//#line 68 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 33:
//#line 70 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 34:
//#line 71 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));val_peek(1).setHijoIzq(val_peek(2));}
break;
case 35:
//#line 73 "Parser.y"
{yyval = val_peek(0);}
break;
case 36:
//#line 74 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 37:
//#line 76 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 38:
//#line 77 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));val_peek(1).setHijoIzq(val_peek(2));}
break;
case 39:
//#line 79 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 40:
//#line 80 "Parser.y"
{yyval = val_peek(0);}
break;
case 41:
//#line 82 "Parser.y"
{yyval = val_peek(0);}
break;
case 42:
//#line 83 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 43:
//#line 85 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 44:
//#line 86 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 45:
//#line 88 "Parser.y"
{yyval = val_peek(0);}
break;
case 46:
//#line 89 "Parser.y"
{yyval = val_peek(0);}
break;
case 47:
//#line 90 "Parser.y"
{yyval = val_peek(0);}
break;
case 48:
//#line 91 "Parser.y"
{yyval = val_peek(0);}
break;
case 49:
//#line 92 "Parser.y"
{yyval = val_peek(0);}
break;
case 50:
//#line 93 "Parser.y"
{yyval = val_peek(0);}
break;
case 51:
//#line 94 "Parser.y"
{yyval = val_peek(0);}
break;
case 52:
//#line 95 "Parser.y"
{yyval = val_peek(0);}
break;
case 53:
//#line 97 "Parser.y"
{yyval = val_peek(0);}
break;
case 54:
//#line 98 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 55:
//#line 100 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 56:
//#line 101 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 57:
//#line 102 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 58:
//#line 103 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 59:
//#line 106 "Parser.y"
{yyval = val_peek(0);}
break;
case 60:
//#line 107 "Parser.y"
{yyval = val_peek(0); val_peek(0).setHijoIzq(val_peek(1));}
break;
case 61:
//#line 109 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 62:
//#line 110 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 63:
//#line 111 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 64:
//#line 112 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0));}
break;
case 65:
//#line 113 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoIzq(val_peek(2)); val_peek(1).setHijoDer(val_peek(0));}
break;
case 66:
//#line 114 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 67:
//#line 115 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 68:
//#line 116 "Parser.y"
{yyval = val_peek(2); val_peek(0).setHijoIzq(val_peek(1)); val_peek(2).setHijoDer(val_peek(0));}
break;
case 69:
//#line 119 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoIzq(val_peek(0));}
break;
case 70:
//#line 120 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoIzq(val_peek(0));}
break;
case 71:
//#line 121 "Parser.y"
{yyval = val_peek(0);}
break;
case 72:
//#line 124 "Parser.y"
{yyval = val_peek(0);}
break;
case 73:
//#line 125 "Parser.y"
{yyval = val_peek(1); val_peek(1).setHijoDer(val_peek(0)); val_peek(1).setHijoIzq(val_peek(2));}
break;
case 74:
//#line 129 "Parser.y"
{yyval = val_peek(0);}
break;
case 75:
//#line 130 "Parser.y"
{yyval = val_peek(0);}
break;
case 76:
//#line 131 "Parser.y"
{yyval = val_peek(0);}
break;
case 77:
//#line 132 "Parser.y"
{yyval = val_peek(0);}
break;
case 78:
//#line 133 "Parser.y"
{yyval = val_peek(0);}
break;
//#line 894 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################
