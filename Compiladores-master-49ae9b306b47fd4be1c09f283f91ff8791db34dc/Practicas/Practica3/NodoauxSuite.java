import java.util.*;

public class NodoauxSuite extends Nodo{

  public NodoauxSuite(String valor){
    this.valor = valor;
    this.listaHijos = new ArrayList<Nodo>();
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public void setHijo(Nodo h){
    this.listaHijos.add(h);
  }

  public void setHijoRecursivo(Nodo h){
    for (Nodo n: h.listaHijos) {
      this.listaHijos.add(n);
    }
  }

  public void imprime(){
    System.out.print("("+this.valor);
    for (Nodo h : this.listaHijos) {
      System.out.print("(");
      h.imprime();
      System.out.print(")");
    }
    System.out.print(")");
  }

}
