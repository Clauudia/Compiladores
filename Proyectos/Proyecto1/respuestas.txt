Investigar como funciona la funcionalidad de contextos que proporciona jflex
	Se declaran de la siguiente manera:

	%s[tate] "state identifier" [, "state identifier", ... ] for inclusive or
	%x[state] "state identifier" [, "state identifier", ... ] for exlusive states

	sirven para restringir el conjunto de expresiones regulares que reconozcan la entrada actual 
	Una expresion regular solo puede ser reconocida cuando su conjunto de estados lexicos incluye el estado lexico activo del scanner o si su
	conjunto de estados lexicos asociados esta vacio y el estado actual es  inclusivo que es la unica diferencia entre este tipo de estados, las reglas con un conjunto vacio de estados.
	El estado yyinitial es el estdo inicial y es inclusivo, ademas siempre se ejecuta por default
	El estado actual puede ser modificado al momento con la accion yybegin()
	O sea podemos definir que algunas reglas solo sean reconocidas en ciertos estados del scanner o excluirlas de otros.

¿Como uso la funcionalidad de depuracion que provee jflex?
	Podemos usar la bandera --debug o la opcion %option debug lo que al ejecutar devuelve 
	-accepting rule at line 53 ("the matched text")
	donde el numero de linea es la ubicacion de la regla definida en el archivo que define el scanner,
	tambien envia mensajes cuando el scanner acepta alguna regla, llega al final de la entrada, o al fin del archivo.

En el uso del lenguaje, ¿La indentación significatia facilita echar código?
	Si, ya que es mucho más fácil identificar los bloques de código y seguir el orden de lo que vamos escribiendo.

En la implementación, ¿La indentación significativa facilita el reconocimiento de bloques? 
	Si, ya que cada bloque va a alineado de acuerdo a donde empieza y donde termina y esto hace más fácil el seguimiento de los bloques.
