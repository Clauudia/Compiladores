
/* globals to track current indentation */
import java.util.ArrayList;

%%

/*Analizador Léxico para una versión minimizada de python*/

%public
%class Flexer
%standalone

%{
ArrayList<Integer> indents = new ArrayList();
int linea = 0;
int indentAnterior = 0;
int indentActual = 0;   /* guardamos la linea actual */
int nivelIndent = 0;          /* nivel de indentacion en el parser */
StringBuffer string = new StringBuffer();
%}

%x INDENT 
%s normal 

/*Definimos la gramática con expresiones regulares*/
INDENT = [\t]
ESPACIO = "    "
NEWLINE = [\n]
IDENTIFICADOR = ([a-zA-Z]|_)([a-zA-Z]|[0-9]|_)*
BOOLEANO = True|False
ENTERO = [1-9][0-9]*|0
REAL = ([1-9][0-9]*|0)("."[0-9]+)
KEYWORD = and|or|not|for|while|if|else|elif|print|in
OPERADOR = \+|\-|\*|\**|\/|\/\/|%|>|<|">="|"<="|"=="|"!="|"-="|"+="
SEPARADOR = \)|\(|:|;|"="
MAS = '+'
MENOS = '-'
IGUAL = '='
IGNORAR = \|''|">*"
CADENA = [^ IGNORAR]

%x CADENA
/*Identificamos cuales son enteros, saltos de linea, etc*/
%%
\"      	  {string.setLength(0); yybegin(CADENA);}
{ENTERO}	  { System.out.print("ENTERO("+yytext()+")"); }
{REAL}		  { System.out.print("REAL("+yytext()+") "); }

{KEYWORD}	  { 
				System.out.print("KEYWORD("+yytext()+")");
			  }
\n[\t]*			{
					linea++;indents.add(indentActual);indentAnterior = indentActual;indentActual = 0;System.out.println("(NEWLINE)");
					if(yylength() > 1){
						indentActual = indentActual +(yylength()-1);
						if(indentActual > indentAnterior){
					nivelIndent++;
					System.out.print("(INDENT(TAB/ "+nivelIndent+"))");
				}else if(indentActual < indentAnterior){
					nivelIndent--;
					System.out.print("(DEDENT("+nivelIndent+"))");
				}
					}
					if(yylength() == 1){
					if(indentActual < indentAnterior){
					nivelIndent=0;
					System.out.print("(DEDENT("+nivelIndent+"))");
					}
					} 	
				}
\n[ ]+			{
					
		    	linea++;indents.add(indentActual);indentAnterior = indentActual;indentActual = 0;System.out.println("(NEWLINE)");
				indentActual = indentActual +(yylength()-1)/4;
				if(indentActual > indentAnterior){
				nivelIndent++;
				System.out.print("(INDENT(ESP/ "+nivelIndent+"))");
				}else if(indentActual < indentAnterior){
				nivelIndent--;
				System.out.print("(DEDENT("+nivelIndent+"))");
				}				
		    	}
{INDENT}      { 
				indentActual++;
				if(indentActual > indentAnterior){
					nivelIndent++;
					System.out.print("(INDENT(TAB/NIVEL: "+nivelIndent+"))");
				}else if(indentActual < indentAnterior){
					nivelIndent--;
					System.out.print("(DEDENT(NIVEL: "+nivelIndent+")");
				}
				}
{NEWLINE}	  { linea++;indents.add(indentActual);indentAnterior = indentActual;indentActual = 0;System.out.println("NEWLINE");}
{OPERADOR}    { System.out.print("OPERADOR("+yytext()+")"); }
{SEPARADOR}   { System.out.print("SEPARADOR("+yytext()+")"); }
{BOOLEANO}    { System.out.print("BOOLEANO("+yytext()+")"); }
{IDENTIFICADOR} { System.out.print("IDENTIFICADOR("+yytext()+")"); }
<CADENA>{
	\"                           {
								   yybegin(YYINITIAL);
								   System.out.print("(CADENA("+string.toString()+"))"); 
                                 }
    [^\n\r\"\\]+                 { string.append( yytext() ); }
  \\t                            { string.append('\t'); }
  \\n                            { string.append('\n'); }

  \\r                            { string.append('\r'); }
  \\\"                           { string.append('\"'); }
  \\                             { string.append('\\'); } 

}


