import java.util.Hashtable;

public class TablaDeSimbolos{
	Hashtable tablaSim;
	//Constructor
	public TablaDeSimbolos(){
		//Creamos la tabla de símbolos
		 tablaSim = new Hashtable<String, String>();
	}

	//Método que regresa el valor asociado a name en la tabla de símbolos
	//o null en caso de que no haya sido encontrado en la tabla
	public String lookup(String name){
		if(!this.tablaSim.containsKey(name)) {
			return null;
		}
		return (String)this.tablaSim.get(name);
	}

	//Método que guarda info en tabla(name)
	public void insert(String name, String info){
		this.tablaSim.put(name,info);

	}
}