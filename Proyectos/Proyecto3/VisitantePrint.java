public class VisitantePrint implements Visitante{
TestTipos r;
TablaDeSimbolos t;

public VisitantePrint(){

	t= new TablaDeSimbolos();
	r= new TestTipos();
}


	public void visita(Nodo n){
		System.out.print("Nodo Genérico");
	}

	public void visita(NodoAnd n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
		}
		System.out.print("Nodo and");
	}

	public void visita(NodoBoolean n){
		System.out.print("Nodo booleano");
	}

	public void visita(NodoCadena n){
		System.out.print("Nodo cadena");
	}

	public void visita(NodoDistinto n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo distinto");
	}

	public void visita(NodoDiv n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
		}
		System.out.print("Nodo división");
	}

	public void visita(NodoDivEnt n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
		}
		System.out.print("Nodo División entera");
	}

	public void visita(NodoDosPuntos n){
		System.out.print("Nodo dos puntos");
	}

	public void visita(NodoElIf n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if (n.getHijoIzq() instanceof NodoID) {
        String hi = t.lookup(n.getHijoIzq().getValor());
        if(hi == null){
          System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          System.exit(1);
        }
      }
		System.out.print("Nodo elif");
	}

	public void visita(NodoElse n){
		n.getHijoDer().acepta(this);
		System.out.print("Nodo else");
	}

	public void visita(NodoEntero n){
		System.out.print("Nodo entero");
	}

	public void visita(NodoIgual n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo eq");
	}

	public void visita(NodoID n){
		System.out.print("Nodo id");
	}

	public void visita(NodoIf n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if (n.getHijoIzq() instanceof NodoID) {
        	String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
      	}
		System.out.print("Nodo if");
	}

	public void visita(NodoEq n){
		if(n.getHijoIzq() instanceof NodoID){
			if(n.getHijoDer() instanceof NodoEntero){
				t.insert(n.getHijoIzq().getValor(), "Entero");
			}
			else if(n.getHijoDer() instanceof NodoReal){
				t.insert(n.getHijoIzq().getValor(), "Real");
			}
			else if(n.getHijoDer() instanceof NodoCadena){
				t.insert(n.getHijoIzq().getValor(), "Cadena");
			}
			else if(n.getHijoDer() instanceof NodoBoolean){
				t.insert(n.getHijoIzq().getValor(), "Booleano");
			}
			else if(n.getHijoDer() instanceof NodoID){
				String type = t.lookup(n.getHijoDer().getValor());
				if(t == null){
					System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          			System.exit(1);
				}
			}
			else{
				n.getHijoDer().acepta(this);
				t.insert(n.getHijoIzq().getValor(), n.getHijoDer().getTipo());
			}
		}
		else
			System.out.println("Asignacion invalida");
		System.out.print("Nodo asignacion");
	}

	public void visita(NodoIn n){
		System.out.print("Nodo In");
	}

	public void visita(NodoPrint n){
    	if (n.getHijoDer() instanceof NodoID) {
    		n.getHijoDer().acepta(this);
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
      	}
      	else{
      		n.getHijoDer().acepta(this);
      	}
		System.out.print("Nodo print");
	}

	public void visita(NodoMas n){
		if(n.getHijoIzq() == null){
			if (n.getHijoDer() instanceof NodoID) {
        		String hd = t.lookup(n.getHijoDer().getValor());
        		if(hd == null){
        			System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          			System.exit(1);
        		}
        	n.setTipo(r.operacionUnaria(hd,n.getValor()));
      		}
      		else {
      			n.getHijoDer().acepta(this);
        		if(n.getHijoDer() instanceof NodoEntero){
        			n.setTipo(r.operacionUnaria("Entero",n.getValor()));
        		}
        		else if(n.getHijoDer() instanceof NodoReal){
        			n.setTipo(r.operacionUnaria("Real",n.getValor()));
        		}
        		else if(n.getHijoDer() instanceof NodoCadena){
        			n.setTipo(r.operacionUnaria("Cadena",n.getValor()));
        		}
        		else if(n.getHijoDer() instanceof NodoBoolean){
        			n.setTipo(r.operacionUnaria("Booleano",n.getValor()));
        		}
        		else{
        			System.out.println("Operacion no valida");
          			System.exit(1);
        		}
      		}
      	}
      	else{
      		n.getHijoIzq().acepta(this);
      		n.getHijoDer().acepta(this);
      		n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
      	}
		System.out.print("Nodo mas");
	}

	public void visita(NodoMasIgual n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID) {
			n.getHijoIzq().acepta(this);
			String hi = t.lookup(n.getHijoIzq().getValor());
        		if(hi == null){
          			System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          			System.exit(1);
        		}
				if(n.getHijoDer() instanceof NodoID){
					String hd = t.lookup(n.getHijoDer().getValor());
        			if(hd == null){
          				System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          				System.exit(1);
        			}
				}
				else if(n.getHijoDer() instanceof NodoEntero ||n.getHijoDer() instanceof NodoReal || n.getHijoDer() instanceof NodoCadena ||n.getHijoDer() instanceof NodoBoolean){
					System.out.println("+= valido");
				}
		}
		else{
			System.out.println("+= no valido");
			System.exit(1);
		}
		System.out.println("Nodo masIgual");
	}

	public void visita(NodoMayor n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo Mayor");
	}

	public void visita(NodoMayorIgual n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo Mayor Igual");
	}

	public void visita(NodoMenor n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo menor");
	}

	public void visita(NodoMenorIgual n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo menor igual");
	}

	public void visita(NodoMenos n){
		if(n.getHijoIzq() == null){
			if (n.getHijoDer() instanceof NodoID) {
        		String hd = t.lookup(n.getHijoDer().getValor());
        		if(hd == null){
        			System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          			System.exit(1);
        		}
        	n.setTipo(r.operacionUnaria(hd,n.getValor()));
      		}
      		else {
      			n.getHijoDer().acepta(this);
        		if(n.getHijoDer() instanceof NodoEntero){
        			n.setTipo(r.operacionUnaria("Entero",n.getValor()));
        		}
        		else if(n.getHijoDer() instanceof NodoReal){
        			n.setTipo(r.operacionUnaria("Real",n.getValor()));
        		}
        		else if(n.getHijoDer() instanceof NodoCadena){
        			n.setTipo(r.operacionUnaria("Cadena",n.getValor()));
        		}
        		else if(n.getHijoDer() instanceof NodoBoolean){
        			n.setTipo(r.operacionUnaria("Booleano",n.getValor()));
        		}
        		else{
        			System.out.println("Operacion no valida");
          			System.exit(1);
        		}
      		}
      	}
      	else{
      		n.getHijoIzq().acepta(this);
      		n.getHijoDer().acepta(this);
      		n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
      	}
		System.out.print("Nodo menos");
	}

	public void visita(NodoMenosIgual n){
		if(n.getHijoIzq() instanceof NodoID) {
			String hi = t.lookup(n.getHijoIzq().getValor());
        		if(hi == null){
          			System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          			System.exit(1);
        		}
				if(n.getHijoDer() instanceof NodoID){
					String hd = t.lookup(n.getHijoDer().getValor());
        			if(hd == null){
          				System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          				System.exit(1);
        			}
				}
				else if(n.getHijoDer() instanceof NodoEntero ||n.getHijoDer() instanceof NodoReal || n.getHijoDer() instanceof NodoCadena ||n.getHijoDer() instanceof NodoBoolean){
					System.out.println("-= valido");
				}
		}
		else{
			System.out.print("-= no valido");
			System.exit(1);
		}
	}

	public void visita(NodoMod n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			System.out.println("Operacion no valida");
          		System.exit(1);
		}
		System.out.print("Nodo mod");
	}

	public void visita(NodoNewLine n){
		
	}

	public void visita(NodoNot n){
		System.out.print("Nodo not");
	}

	public void visita(NodoNotIn n){
		System.out.print("Nodo NotIn");
	}

	public void visita(NodoOperador n){
		System.out.print("Nodo operador");
	}

	public void visita(NodoOr n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
		}
		System.out.print("Nodo Or");
	}

	public void visita(NodoPor n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
		}
		System.out.print("Nodo Por");
	}

	public void visita(NodoPot n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if(n.getHijoIzq() instanceof NodoID && n.getHijoDer() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	n.setTipo(r.operacionBinaria(hi,hd,n.getValor()));
		}
		else if(n.getHijoIzq() instanceof NodoID){
			String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoDer() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria(hi,"Entero",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria(hi,"Real",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria(hi,"Cadena",n.getValor()));
        	}
        	else if(n.getHijoDer() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria(hi,"Booleano",n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else if(n.getHijoDer() instanceof NodoID){
			String hd = t.lookup(n.getHijoDer().getValor());
        	if(hd == null){
          		System.out.println("El identificador " + n.getHijoDer().getValor() + " no fue declarado");
          		System.exit(1);
        	}
        	if(n.getHijoIzq() instanceof NodoEntero){
        		n.setTipo(r.operacionBinaria("Entero",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoReal){
        		n.setTipo(r.operacionBinaria("Real",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoCadena){
        		n.setTipo(r.operacionBinaria("Cadena",hd,n.getValor()));
        	}
        	else if(n.getHijoIzq() instanceof NodoBoolean){
        		n.setTipo(r.operacionBinaria("Booleano",hd,n.getValor()));
        	}
        	else{
        		System.out.println("Operacion no valida");
          		System.exit(1);
        	}
		}
		else{
			n.setTipo(r.operacionBinaria(n.getHijoIzq().getTipo(),n.getHijoDer().getTipo(),n.getValor()));
		}
		System.out.print("Nodo Potencia");
	}

	public void visita(NodoPuntoyComa n){
		System.out.print("Nodo Punto y coma");
	}

	public void visita(NodoRaiz n){
		for(Nodo i : n.getHijos()){
			i.acepta(this);
		}
	}

	public void visita(NodoReal n){
		System.out.print("Nodo Real");
	}

	public void visita(NodoSeparador n){
		System.out.print("Nodo Separador");
	}

	public void visita(NodoWhile n){
		if(n.getHijoIzq() != null && n.getHijoDer() != null){
			n.getHijoIzq().acepta(this);
			n.getHijoDer().acepta(this);
		}
		if (n.getHijoIzq() instanceof NodoID) {
        	String hi = t.lookup(n.getHijoIzq().getValor());
        	if(hi == null){
          		System.out.println("El identificador " + n.getHijoIzq().getValor() + " no fue declarado");
          		System.exit(1);
        	}
      	}
		System.out.print("Nodo while");
	}

	public void visita(NodoauxIF n){
		n.acepta(this);
		System.out.print("Nodo auxIf");
	}

	public void visita(NodoauxSuite n){
		n.acepta(this);
		System.out.print("Nodo suite");
	}

}