import java.util.ArrayList;

public class TestTipos{

  ArrayList<String> tipos;


  //las tablas estan hechas de la siguiente forma
  //		Booleano Entero Real Cadena
  //Booleano
  //Entero
  //Real
  //Cadena
  //
  //Debemos definir valores para cada uno, por simplicidad los numeramos
  //-1 sera tipo invalido
  //0-booleano
  //1-entero
  //2-real
  //3-cadena

  //Tabla para la operación de suma
  private final byte[][] suma = new byte[][]{
    {0, 1, 2, 3},
    {1, 1, 2, 3},
    {2, 2, 2, 3},
    {3, 3, 3, 3}
  };

  //Tabla para la operación de resta
  private final byte[][] resta = new byte[][]{
    {0, 1, 2, 3},
    {1, 1, 2, 3},
    {2, 2, 2, 3},
    {3, 3, 3, 3}
  };

  //Tabla para la operación de multiplicación
  private final byte[][] mult = new byte[][]{
    {1, 1, 2, 3},
    {1, 1, 2, 3},
    {2, 2, 2, -1},
    {3,3,-1,-1}
  };

  //Tabla para la operación de potencia
  private final byte[][] pot = new byte[][]{
    {1, 1, 2, -1},
    {1, 1, 2, -1},
    {2, 2, 2, -1},
    {-1,-1,-1,-1}
  };

  //Tabla para la operación de division
  //recordar que entero con entero nos devuelve la parte entera nadamas
  private final byte[][] div = new byte[][]{
    {1, 1, 2, -1},
    {1, 1, 2, -1},
    {2, 2, 2, -1},
    {-1,-1,-1,-1}
  };

  //Tabla para la operación de cociente
  private final byte[][] coc = new byte[][]{
    {1, 1, 2, -1},
    {1, 1, 2, -1},
    {2, 2, 2, -1},
    {-1,-1,-1,-1}
  };

  //Tabla para la operación de modulo
  private final byte[][] mod = new byte[][]{
    {1, 1, 2, -1},
    {1, 1, 2, -1},
    {2, 2, 2, -1},
    {-1,-1,-1,-1}
  };

  //Tabla para operaciones de comparaciones
  private final byte[][] com = new byte[][]{
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0},
    {0, 0, 0, 0}
  };

  //Tabla para la operación de and
  private final byte[][] and = new byte[][]{
    {0, 1, 2, 3},
    {0, 1, 2, 3},
    {0, 1, 2, 3},
    {0, 1, 2, 3}
  };

  //Tabla para la operación de or
  private final byte[][] or = new byte[][]{
    {0, 0, 0, 0},
    {1, 1, 1, 1},
    {2, 2, 2, 2},
    {3, 3, 3, 3}
  };

  //Tabla para la operacion de + y - unaria
  private final byte[] unarios = new byte[] {1,1,2,-1};

  //Tabla para la operacion not (todo queda booleano)
  private final byte[] not = new byte[] {0,0,0,0};

  public TestTipos() {
    tipos = new ArrayList<String>();
    tipos.add("Booleano");
    tipos.add("Entero");
    tipos.add("Real");
    tipos.add("Cadena");
  }

  //método que regresa el tipo de una operación binaria
  public String operacionBinaria (String izquierdo, String derecho, String operador) {
    String output = "";
    if (tipos.contains(izquierdo) && tipos.contains(derecho)){
      byte tipo = 0;
      switch (operador) {
        case "+": 
        	tipo = suma[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
           	break;
        case "-": 
        	tipo = resta[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "*": 
        	tipo = mult[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "**": 
        	tipo = pot[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "/": 
        	tipo = div[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "//": 
        	tipo = coc[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "%": 
        	tipo = mod[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "<": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case ">": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "<=": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case ">=": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "==": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "!=": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "<>": 
        	tipo = com[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "and": 
        	tipo = and[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        case "or": 
        	tipo = or[tipos.indexOf(izquierdo)][tipos.indexOf(derecho)];
            break;
        default: 
        	System.out.println("Caso default :p");
        	break;
      }
      if (tipo >= 0) {
        output = tipos.get(tipo);
      }
      else {
        System.out.println("TIPOS INCOMPATIBLES");
        System.exit(1);
      }
    }
    return output;
  }

  public String operacionUnaria (String tipoExp, String operador) {
    String output = "";
    if (tipos.contains(tipoExp)){
      byte tipo = 0;
      switch (operador) {
        case "+": 
        	tipo = unarios[tipos.indexOf(tipoExp)];
            break;
        case "-": 
        	tipo = unarios[tipos.indexOf(tipoExp)];
            break;
        case "not": 
        	tipo = not[tipos.indexOf(tipoExp)];
            break;
        default: 
        	System.out.println("Caso default :p");
        	break;
      }
      if (tipo >= 0) {
        output = tipos.get(tipo);
      }
      else {
        System.out.println("TIPOS INCOMPATIBLES");
        System.exit(1);
      }
    }
    return output;
  }

}