public class NodoCadena extends Nodo{

  public NodoCadena(String valor){
    this.valor = valor;
  }

  public NodoCadena(String valor, Nodo izq, Nodo der) {
    this.valor = valor;
    this.izquierdo = izq;
    this.derecho = der;
  }

  public String getValor(){
    return valor;
  }

  public void setValor(String v) {
    this.valor = v;
  }

  public Nodo getHijoIzq(){
    return this.izquierdo;
  }

  public void setHijoIzq(Nodo i){
    this.izquierdo = i;
  }

  public Nodo getHijoDer(){
    return this.derecho;
  }

  public void setHijoDer(Nodo d){
    this.derecho = d;
  }

  public void acepta(Visitante v){
  v.visita(this);
}

}
