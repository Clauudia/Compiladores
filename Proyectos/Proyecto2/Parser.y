%{
  import java.lang.Math;
  import java.io.*;
%}
/* YACC Declarations */
%token NEWLINE IDENTIFIER ENTERO REAL CADENA S ENDOFFILE
%token BOOLEAN 
%token PRINT INDENT DEDENT
%token MAS MENOS POR POTENCIA DIV  /* + | - | * | ** | / */
%token DIVENTERA MODULO EQ /* // | % | =*/
%token MASIGUAL MENOSIGUAL DISTINTO IGUALIGUAL MENORIGUAL MAYORIGUAL MENOR MAYOR EQ
%token PYCOMA DOSPUNTOS PD PI 
%token IN ELIF ELSE IF WHILE FOR NOT OR AND NOTIN
%right ELIF ELSE
/* Grammar follows */
%%

file_input: { System.out.println("Reconocimiento exitoSO");}
            |aux2 {System.out.println("Reconocimiento exitoso");}
;
aux2: NEWLINE
      |stmt
      |aux2 NEWLINE
      |aux2 stmt
;
stmt: simple_stmt
      | compound_stmt
;
simple_stmt: small_stmt NEWLINE
;
small_stmt: expr_stmt
            | print_stmt
;
expr_stmt: test
	|test aux3
;
aux3: EQ test
      | augassign test
;
augassign: MASIGUAL
          | MENOSIGUAL
;
print_stmt: PRINT test
;
compound_stmt: if_stmt
                | while_stmt
;
/*if_stmt: 'if' test ':' suite ('elif' test ':' suite)* ['else' ':' suite]*/
if_stmt: IF test DOSPUNTOS suite
          | IF test DOSPUNTOS suite ELSE DOSPUNTOS suite
          | IF test DOSPUNTOS suite aux20 ELSE DOSPUNTOS suite
;
aux20: ELIF test DOSPUNTOS suite
      | ELIF test DOSPUNTOS suite aux20
;

while_stmt: WHILE test DOSPUNTOS suite
;
suite: simple_stmt 
      | NEWLINE INDENT aux00 DEDENT
;

aux00: stmt
      | aux00 stmt
;
test: or_test
;
or_test: and_test
          | and_test aux5
;
aux5: OR and_test
      | aux5 OR and_test
;
and_test: not_test
          | not_test aux6
;
aux6: AND not_test
      | aux6 AND not_test
;
not_test: NOT not_test
          | comparison
;
comparison: expr
            | expr aux7
;
aux7: comp_op expr
      | comp_op expr aux7
;
comp_op: MENOR
        | MAYOR
        | IGUALIGUAL
        | MAYORIGUAL
        | MENORIGUAL
        | DISTINTO
        | IN
        | NOTIN
;
expr: term
      | term aux8
;
aux8: MAS term
      | MENOS term
      | MAS term aux8
      | MENOS term aux8
;
/* term: factor (('*'|'/'|'%'|'//') factor)* */
term:  factor
      | factor aux9
;
aux9:  POR factor
     | DIVENTERA factor
     | MODULO factor
     | DIV factor
     | aux9 POR factor 
     | aux9 DIVENTERA factor
     | aux9 MODULO factor
     | aux9 DIV factor
;
/* factor: ('+'|'-') factor | power */
factor: MAS factor
	| MENOS factor
	| power
;
/* power: atom ['**' factor] */
power:  atom
      | atom POTENCIA factor
;

/* atom: IDENTIFIER | INTEGER | STRING | FLOAT */
atom:  IDENTIFIER
     | ENTERO
     | CADENA
     | REAL
     | BOOLEAN
;
%%
/* a reference to the lexer object */
private Flexer lexer;

/* interface to the lexer */
private int yylex () {
    int yyl_return = -1;
    try {
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }
    return yyl_return;
}

/* error reporting */ 
public void yyerror (String error) {
    System.err.println ("Error: " + error);
}

/* lexer is created in the constructor */
public Parser(Reader r) {
    lexer = new Flexer(r, this);
    yydebug = true;
}

/* that's how you use the parser */
public static void main(String args[]) throws IOException {
    Parser yyparser = new Parser(new FileReader(args[0]));
    yyparser.yyparse();    
}
